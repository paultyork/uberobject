﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Diagnostics;

namespace SystemSharp
{
    /// <summary>
    /// <para>
    /// Provides a very robust base class and/or utility class, supporting deep property cloning that is
    /// 100% configurable to include reference types and arbitrarily hierarchically deep properties, change 
    /// tracking with multi-level undo (also 100% configurable as to the hierarchical breadth and depth of 
    /// the tracking--ideal for data bound forms enabling you to easily cancel edits), CopyTo and UpdateFrom
    /// methods to allow you to easily transfer the values from one object to another, and property-by-property
    /// (memberwise) object equivalance checking (again 100% configurable to allow inclusion or exclusion of 
    /// both shallow and deep properties).
    /// </para>
    /// <para>
    /// The updating, copying and cloning functionality will not only perform deep copies/clones, but the 
    /// copies will be true value copies, not reference copies--very critical when dealing with objects that 
    /// contain collections of objects. With reference cloning (ala MemberwiseClone), if you copy a collection 
    /// of objects, you get a pointer to the SAME list with the SAME objects (change an object in one you've
    /// done so in the other, as well). Here you get a new collection with value-wise identical objects.
    /// </para>
    /// <para>
    /// Due to the aggressive use of static caching and the generation of dynamic (i.e., native MSIL) property 
    /// copy and equivalance determination functions, all of this is provided with nearly no overhead compared 
    /// to writing this type of functionality natively into your classes. 
    /// </para>
    /// </summary>
    public class UberObject : System.Object, ICloneable, IEquatable<UberObject>, IEqualityComparer
    {
        
        #region Public Properties

        /// <summary>
        /// Read-only. Contains a clone of an object taken at the time BeginTrackingChanges() is called.
        /// The value is reset if any subsequent callse are made to BeginTrackingChanges(). It is set to
        /// null after a call to StopTrackingChanges() unless the optional keepOriginal parameter is 
        /// passed as true.
        /// </summary>
        public UberObject OriginalObject { get; private set; }
        
        /// <summary>
        /// Read-only. True if the UberObject instance is currently tracking changes. Otherwisa false.
        /// </summary>
        public bool IsTrackingChanges { get; private set; }

        /// <summary>
        /// Read-only. Indicates whether the current object has been modified from its original
        /// state. It reflects only whether the object has been modified since the last call to
        /// BeginTrackingChanges(). If the object has never been put into change tracking mode,
        /// then IsDirty will always return false regardless of whether changes have been made to
        /// the object. Likewise, if StopTrackingChanges() is called to take the object out of
        /// change tracking mode, IsDirty will reflect the dirty state of the object at the time
        /// of the call to StopTrackingChanges(). Note that if the object is subsequently put back
        /// into change tracking mode, this property will be reset to false.
        /// </summary>
        public bool IsDirty
        {
            get
            {
                if (IsTrackingChanges)
                {
                    return !_Equals(this, OriginalObject, _trackedTypeInfo);
                }
                return _isDirty;
            }
            private set
            {
                _isDirty = value;
            }
        }
        private bool _isDirty;
        
        /// <summary>
        /// Provides a guaranteed unique ID for each individual UberObject instance. Useful for 
        /// change tracking internally, but also potentially useful in a number of other external 
        /// situations, as well. This property is writeable to allow for full cloning, but in
        /// general it should considered read-only.
        /// </summary>
        public String UberGUID
        {
            get
            {
                // Only create the Guid when first requested as this is somewhat costly
                if (_UberGUID == null) _UberGUID = Guid.NewGuid().ToString();
                return _UberGUID;
            }
            set
            {
                _UberGUID = value;
            }
        }
        private String _UberGUID;
        
        #endregion

        #region Equals Methods

        #region IEquatable<UberObject> Methods

        /// <summary>
        /// Unlike the base Object.Equals(Object) method which performs a hash check to
        /// determine if two objects are the same, this performs a property-by-property
        /// value check to determine whether two objects are value-wise equivalent.
        /// This makes it useable as a "default" IEquatable interface.
        /// </summary>
        /// <param name="toCompare">The object to compare.</param>
        /// <returns>True if the objects are value-wise equivalent. Otherwise false.</returns>
        public virtual bool Equals(UberObject toCompare)
        {
            return _Equals(this, toCompare, TypeInfo.GetTypeInfo(this, null));
        }

        #endregion

        #region IEqualityComparer Methods

        /// <summary>
        /// Provides an IEqualityComparer compatible Equals implementation that
        /// determines if two objects are the same by performing a property-by-property
        /// value check to determine whether two objects are value-wise equivalent.
        /// </summary>
        /// <param name="toCompare">The object to compare.</param>
        /// <returns>True if the objects are value-wise equivalent. Otherwise false.</returns>
        public virtual new bool Equals(object object1, object object2)
        {
            return _Equals(object1, object2, TypeInfo.GetTypeInfo(object1, null));
        }

        /// <summary>
        /// Provides an IEqualityComparer compatible GetHashCode implementation that
        /// simply calls the object's base GetHashCode method.
        /// </summary>
        /// <param name="toCompare">The object to compare.</param>
        /// <returns>True if the objects are value-wise equivalent. Otherwise false.</returns>
        public virtual int GetHashCode(object forObject)
        {
            return forObject.GetHashCode();
        }

        #endregion

        #region Other Public Equals Methods

        /// <summary>
        /// Unlike the base Object.Equals(Object) method which performs a hash check to
        /// determine if two objects are the same, this performs a property-by-property
        /// value check to determine whether two objects are value-wise equivalent. By
        /// default, this will only check the equivalence of properties that are value 
        /// types or strings. However, you can specify an arbitrarily deep list of
        /// reference type properties that you wish to include in the value comparison
        /// using the referenceProperties parameter (see the parameter description).
        /// </summary>
        /// <param name="toCompare">The object to compare.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the comparison
        /// process should compare by-value. If the property is an object, the 
        /// comparison will succeed if the referenced objects are value-wise 
        /// equivalent. Similarly, if the property is a collection, the comparison
        /// will succeed if the collections contain value-wise equivalent objects
        /// (order dependent for arrays and generic Lists).
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can compare an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could compare the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <returns>True if the objects are value-wise equivalent. Otherwise false.</returns>
        public virtual bool Equals(object toCompare, string referenceProperties)
        {
            return _Equals(this, toCompare, TypeInfo.GetTypeInfo(this, referenceProperties));
        }

        /// <summary>
        /// Unlike the base Object.Equals(Object) method which performs a hash check to
        /// determine if two objects are the same, this performs a property-by-property
        /// value check to determine whether two objects are value-wise equivalent. By
        /// default, this will only check the equivalence of properties that are value 
        /// types or strings. However, you can specify an arbitrarily deep list of
        /// reference type properties that you wish to include in the value comparison
        /// using the referenceProperties parameter (see the parameter description).
        /// </summary>
        /// <param name="toCompare">The object to compare.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the comparison
        /// process should compare by-value. If the property is an object, the 
        /// comparison will succeed if the referenced objects are value-wise 
        /// equivalent. Similarly, if the property is a collection, the comparison
        /// will succeed if the collections contain value-wise equivalent objects
        /// (order dependent for arrays and generic Lists).
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can compare an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could compare the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// ignore certain properties when comparing for equivalence by specifying
        /// them as part of a comma-separated list passed in as this parameter. Like
        /// the referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from comparison as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        /// <returns>True if the objects are value-wise equivalent. Otherwise false.</returns>
        public virtual bool Equals(object toCompare, string referenceProperties, String excludedProperties)
        {
            return _Equals(this, toCompare, TypeInfo.GetTypeInfo(this, referenceProperties, excludedProperties));
        }

        /// <summary>
        /// A static (utility) method to check two objects for value-wise equivalence.
        /// Unlike the base Object.Equals(Object) method which performs a hash check to
        /// determine if two objects are the same, this performs a property-by-property
        /// value check to determine whether two objects are value-wise equivalent. By
        /// default, this will only check the equivalence of properties that are value 
        /// types or strings. However, you can specify an arbitrarily deep list of
        /// reference type properties that you wish to include in the value comparison
        /// using the referenceProperties parameter (see the parameter description).
        /// </summary>
        /// <param name="firstObject">The first object to compare.</param>
        /// <param name="secondObject">The second object to compare.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the comparison
        /// process should compare by-value. If the property is an object, the 
        /// comparison will succeed if the referenced objects are value-wise 
        /// equivalent. Similarly, if the property is a collection, the comparison
        /// will succeed if the collections contain value-wise equivalent objects
        /// (order dependent for arrays and generic Lists).
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can compare an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could compare the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <returns>True if the objects are value-wise equivalent. Otherwise false.</returns>
        public static bool Equals(object firstObject, object secondObject, string referenceProperties)
        {
            return _Equals(firstObject, secondObject, TypeInfo.GetTypeInfo(firstObject, referenceProperties));
        }

        /// <summary>
        /// A static (utility) method to check two objects for value-wise equivalence.
        /// Unlike the base Object.Equals(Object) method which performs a hash check to
        /// determine if two objects are the same, this performs a property-by-property
        /// value check to determine whether two objects are value-wise equivalent. By
        /// default, this will only check the equivalence of properties that are value 
        /// types or strings. However, you can specify an arbitrarily deep list of
        /// reference type properties that you wish to include in the value comparison
        /// using the referenceProperties parameter (see the parameter description).
        /// </summary>
        /// <param name="firstObject">The first object to compare.</param>
        /// <param name="secondObject">The second object to compare.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the comparison
        /// process should compare by-value. If the property is an object, the 
        /// comparison will succeed if the referenced objects are value-wise 
        /// equivalent. Similarly, if the property is a collection, the comparison
        /// will succeed if the collections contain value-wise equivalent objects
        /// (order dependent for arrays and generic Lists).
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can compare an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could compare the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// ignore certain properties when comparing for equivalence by specifying
        /// them as part of a comma-separated list passed in as this parameter. Like
        /// the referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from comparison as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        /// <returns>True if the objects are value-wise equivalent. Otherwise false.</returns>
        public static bool Equals(object firstObject, object secondObject, string referenceProperties, string excludedProperties)
        {
            return _Equals(firstObject, secondObject, TypeInfo.GetTypeInfo(firstObject, referenceProperties, excludedProperties));
        }

        #endregion

        #region Private Equals Methods

        /// <summary>
        /// Similar in ways to the _CopyTo method, this private method recursively checks
        /// objects and the specified referenceProperties for equivalence.
        /// </summary>
        /// <param name="firstObject"></param>
        /// <param name="secondObject"></param>
        /// <param name="typeInfo"></param>
        /// <returns></returns>
        private static bool _Equals(object firstObject, object secondObject, TypeInfo typeInfo)
        {
            if (typeInfo == null)
                return false;
            
            // Declare a boolean for the return value
            bool equals;

            // Before we begin, we can check to see if the objects both null. If both are null, then 
            // they should be considered equal. If one is but the other isn't then they are not equal.
            if (firstObject == null && secondObject == null) return true;
            if (firstObject == null && secondObject != null) return false;
            if (secondObject == null && firstObject != null) return false;

            // Okay, if the objects are value types (or strings), then we can just compare them.
            if (typeInfo.IsValueType)
                return firstObject.Equals(secondObject);
            
            // Okay, we have two non-null objects. Now check to see if they are standard objects or 
            // collections.
            if (!typeInfo.IsCollection)
            {
                // Okay, it's a standard object.
                // First, let's figure out which properties should be excluded from our comparison.
                String excludedProperties = typeInfo.ExcludedProperties;
                List<String> excludedPropertiesList = null;
                if (excludedProperties != null) excludedPropertiesList = excludedProperties.Split(',').ToList<String>();

                // For UberObjects, we'll automatically ignore the special properties (only necessary
                // for the shallow MemberwiseEquals method, so no need to add these specifically to
                // the excludedPropertiesList, which we will leverage for our referenceProperties checks
                // below).
                if (firstObject is UberObject && secondObject is UberObject)
                    excludedProperties += ",UberGUID,IsDirty,IsTrackingChanges";

                // Now do a MemberwiseEquals on the object. If this returns false,
                // then we're done.
                equals = MemberwiseEquals(firstObject, secondObject, excludedProperties);
                if (!equals) return false;

                // Okay, they base objects are equal, now lets recursively check to see if the
                // specified referenceProperties are also equal.

                // If there are no specified referenceProperties, then we are done.
                if (typeInfo.ReferencePropertiesTypeInfo == null || typeInfo.ReferencePropertiesTypeInfo.Count == 0)
                    return true;

                // Loop through each of the referenceProperties
                foreach (String referencePropertyName in typeInfo.ReferencePropertiesTypeInfo.Keys)
                {
                    // Spit out if the property is in the list of excluded ones
                    if (excludedPropertiesList != null && excludedPropertiesList.Contains<String>(referencePropertyName))
                        continue;

                    // Get the TypeInfo for the name
                    TypeInfo referencePropertyTypeInfo = typeInfo.ReferencePropertiesTypeInfo[referencePropertyName];

                    // Get the property values.
                    Object firstValue = referencePropertyTypeInfo.PropertyInfo.GetValue(firstObject, null);
                    Object secondValue = referencePropertyTypeInfo.PropertyInfo.GetValue(secondObject, null);

                    // Now, armed with these objects, we can simply call this method recursively to 
                    // check them for equivalence.
                    if (!_Equals(firstValue, secondValue, referencePropertyTypeInfo))
                        return false;
                }
            }
            else // IS A COLLECTION
            {
                TypeInfo secondTypeInfo = TypeInfo.GetTypeInfo(secondObject, typeInfo.ReferenceProperties);
                
                // Dictionaries are special. Assuming both are dictionaries, we proceed.
                if (typeInfo.IsDictionary && secondTypeInfo.IsDictionary)
                {
                    // Dictionaries do not have a guaranteed order, but they do provide for easy retrieval
                    // via their keys.
                    IDictionary firstDict = firstObject as IDictionary;
                    IDictionary secondDict = secondObject as IDictionary;

                    // If the counts are different, then they aren't equal
                    if (firstDict.Count != secondDict.Count) return false;

                    // Now we loop through the keys in the first object, check to see if they exist in the second,
                    // and if so, we check the equivalence of their values.
                    foreach (Object key in firstDict.Keys)
                    {
                        if (!secondDict.Contains(key)) return false;
                        if (!_Equals(firstDict[key], secondDict[key], typeInfo.ElementTypeInfo))
                            return false;
                    }
                }
                // All other types we ASSUME that the order is guaranteed and enumerate over them. If each
                // item is identical, then the collections are identical.
                else if (!typeInfo.IsDictionary && secondTypeInfo.IsCollection && !secondTypeInfo.IsDictionary)
                {
                    // Treat both as IEnumerables.
                    IEnumerable firstEnumerable = firstObject as IEnumerable;
                    IEnumerable secondEnumerable = secondObject as IEnumerable;

                    // Get IEnumerators for both
                    IEnumerator firstEnumerator = firstEnumerable.GetEnumerator();
                    IEnumerator secondEnumerator = secondEnumerable.GetEnumerator();

                    bool gotItems = true;

                    do
                    {
                        bool gotFirst = firstEnumerator.MoveNext();
                        bool gotSecond = secondEnumerator.MoveNext();

                        // If the enumerators return different sizes
                        if (gotFirst != gotSecond)
                            return false;

                        gotItems = gotFirst && gotSecond;

                        if (!gotItems)
                            continue;

                        Object firstItem = firstEnumerator.Current;
                        Object secondItem = secondEnumerator.Current;

                        if (!(firstItem == null) && (secondItem == null))
                            return false;

                        if (firstItem != null)
                        {
                            // Now just call _Equals for each array member
                            if (!_Equals(firstItem, secondItem, typeInfo.ElementTypeInfo))
                                return false;
                        }
                    } while (gotItems);
                }
                else
                {
                    String message = "Tried to compare two incompatible collections: " +
                                     typeInfo.Type.ToString() + " and " + secondTypeInfo.Type.ToString();
                    if (_exceptionMode == ExceptionMode.ThrowException)
                    {
                        throw new UnsupportedCollectionTypeException(message);
                    }
                    else
                    {
                        _OnWarning(message);
                        return false;
                    }
                }
            }

            // if we get here, then the objects/collections were equivalent, so return true.
            return true;
        }

        #endregion

        #endregion

        #region Clone Methods

        #region IClonable Methods

        /// <summary>
        /// Create a property-by-property cloned instance of this object.
        /// Essentially performs a MemberwiseClone of the UberObject, though
        /// the new object will NOT contain references to properties that
        /// are not value types or strings and the new object will have a 
        /// different UberGUID from the original so that the original and clone
        /// can be differentiated. This behavior can be customized using 
        /// other Clone() method variations.
        /// </summary>
        /// <returns>The cloned object.</returns>
        public virtual Object Clone()
        {
            return _Clone(TypeInfo.GetTypeInfo(this, null), false);
        }

        #endregion

        #region Other Public Clone Methods

        /// <summary>
        /// Creates a property-by-property clone of an object. By default,
        /// cloning will not copy references to properties that are not
        /// value types or strings. This is by design because you don't 
        /// want to create arbitrarily deep clones to avoid unnecessarily 
        /// massive objects or potential circular references. However, any 
        /// properties that you wish to clone by-value (i.e., clone the 
        /// referenced object) can be specified in the referenceProperties
        /// parameter (see the parameter description).
        /// </summary>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the cloning
        /// process should copy by-value. If the property is an object, the 
        /// property in the clone will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the cloned object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can clone an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could clone the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <returns>The cloned object.</returns>
        public virtual Object Clone(String referenceProperties)
        {
            return _Clone(TypeInfo.GetTypeInfo(this, referenceProperties), false);
        }

        /// <summary>
        /// Creates a property-by-property clone of an object. By default,
        /// cloning will not copy references to properties that are not
        /// value types or strings. This is by design because you don't 
        /// want to create arbitrarily deep clones to avoid unnecessarily 
        /// massive objects or potential circular references. However, any 
        /// properties that you wish to clone by-value (i.e., clone the 
        /// referenced object) can be specified in the referenceProperties
        /// parameter (see the parameter description).
        /// </summary>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the cloning
        /// process should copy by-value. If the property is an object, the 
        /// property in the clone will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the cloned object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can clone an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could clone the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="preserveGuids">
        /// By default, a clone will have a new UberGUID to differentiate itself from 
        /// the original object. Set this value to true to override this behavior.
        /// </param>
        /// <returns>The cloned object.</returns>
        public virtual Object Clone(String referenceProperties, bool preserveGuids)
        {
            return _Clone(TypeInfo.GetTypeInfo(this, referenceProperties), preserveGuids);
        }

        /// <summary>
        /// Create a property-by-property cloned instance of an object.
        /// Essentially performs a MemberwiseClone of the object, though
        /// the new object will NOT contain references to properties that
        /// are not value types or strings and the new object will have a 
        /// different UberGUID from the original so that the original and clone
        /// can be differentiated. This behavior can be customized using 
        /// other CloneObject() method variations.
        /// </summary>
        /// <param name="source">The object to be cloned.</param>
        /// <returns>The cloned object.</returns>
        public static Object CloneObject(Object source)
        {
            if (source == null)
                throw new ArgumentNullException();
            return _CloneObject(source, TypeInfo.GetTypeInfo(source, null), false);
        }

        /// <summary>
        /// Creates a property-by-property clone of an object. By default,
        /// cloning will not copy references to properties that are not
        /// value types or strings. This is by design because you don't 
        /// want to create arbitrarily deep clones to avoid unnecessarily 
        /// massive objects or potential circular references. However, any 
        /// properties that you wish to clone by-value (i.e., clone the 
        /// referenced object) can be specified in the referenceProperties
        /// parameter (see the parameter description).
        /// </summary>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the cloning
        /// process should copy by-value. If the property is an object, the 
        /// property in the clone will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the cloned object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can clone an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could clone the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="source">The object to be cloned.</param>
        /// <returns>The cloned object.</returns>
        public static Object CloneObject(Object source, String referenceProperties)
        {
            if (source == null)
                throw new ArgumentNullException();
            return _CloneObject(source, TypeInfo.GetTypeInfo(source, referenceProperties), false);
        }

        #endregion

        #region Private Clone Methods

        /// <summary>
        /// Internal alternative to public Clone method which eliminates need
        /// translate referenceProperties string into a cache.
        /// </summary>
        /// <param name="referencePropertiesCache"></param>
        /// <param name="preserveGuids"></param>
        /// <returns></returns>
        private Object _Clone(TypeInfo typeInfo, bool preserveGuids)
        {
            return _CloneObject(this, typeInfo, preserveGuids);
        }

        /// <summary>
        /// Internal alternative to public CloneObject method which eliminates need
        /// translate referenceProperties string into a cache.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="referencePropertiesCache"></param>
        /// <param name="preserveGuids"></param>
        /// <returns></returns>
        private static Object _CloneObject(Object source, TypeInfo typeInfo, bool preserveGuids)
        {
            Object clone = CreateObjectInstance(source.GetType());
            _CopyTo(source, clone, typeInfo, false, preserveGuids, false);
            return clone;
        }

        #endregion

        #endregion

        #region CopyTo/UpdateFrom Methods

        #region Instance CopyTo/UpdateFrom Methods

        /// <summary>
        /// Performs a property-by-property copy of this object to another
        /// specified destination object. It will not copy references to 
        /// properties that are not value types or strings. The copy is "smart" 
        /// in the sense that it will only attempt to copy properties that exist 
        /// (and are of compatible types) both in this object and in the 
        /// destination object and then only if they are different (as determined
        /// by a call to the source object's Equals method).
        /// </summary>
        /// <param name="dest">The object to which to copy the properties of this object.</param>
        public virtual void CopyTo(Object dest)
        {
            _CopyTo(this, dest, TypeInfo.GetTypeInfo(this, null), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of this object to another
        /// specified destination object. By default, it will not copy 
        /// references to properties that are not value types or strings. 
        /// However, any properties that you wish to copy by-value (i.e., 
        /// clone the referenced object) can be specified in the 
        /// referenceProperties parameter (see the parameter description).
        /// The copy is "smart" in the sense that it will only attempt to 
        /// copy properties that exist (and are of compatible types) 
        /// both in this object and in the  destination object and then 
        /// only if they are different (as determined by a call to the 
        /// source object's Equals method).
        /// </summary>
        /// <param name="dest">The object to which to copy the properties of this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        public virtual void CopyTo(Object dest, String referenceProperties)
        {
            _CopyTo(this, dest, TypeInfo.GetTypeInfo(this, referenceProperties), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of this object to another
        /// specified destination object. By default, it will not copy 
        /// references to properties that are not value types or strings. 
        /// However, any properties that you wish to copy by-value (i.e., 
        /// clone the referenced object) can be specified in the 
        /// referenceProperties parameter (see the parameter description).
        /// The copy is "smart" in the sense that it will only attempt to 
        /// copy properties that exist (and are of compatible types) 
        /// both in this object and in the  destination object and then 
        /// only if they are different (as determined by a call to the 
        /// source object's Equals method).
        /// </summary>
        /// <param name="dest">The object to which to copy the properties of this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        public virtual void CopyTo(Object dest, String referenceProperties, String excludedProperties)
        {
            _CopyTo(this, dest, TypeInfo.GetTypeInfo(this, referenceProperties, excludedProperties), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of this object to another
        /// specified destination object. By default, it will not copy 
        /// references to properties that are not value types or strings. 
        /// However, any properties that you wish to copy by-value (i.e., 
        /// clone the referenced object) can be specified in the 
        /// referenceProperties parameter (see the parameter description).
        /// The copy is "smart" in the sense that it will only attempt to 
        /// copy properties that exist (and are of compatible types) 
        /// both in this object and in the  destination object and then 
        /// only if they are different (as determined by a call to the 
        /// source object's Equals method).
        /// </summary>
        /// <param name="dest">The object to which to copy the properties of this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        /// <param name="mergeCollections">
        /// By default, if they are specified as one of the referenceProperties and 
        /// if they aren't empty, collections--including arrays, Lists (standard or 
        /// generic and their derivatives), and Dictionaries (standard or generic and 
        /// their derivatives--in the destination object will be cleared and repopulated 
        /// with clones of the objects in the source collection. However, if the 
        /// collections are both Dictionaries or generic Lists of UberObjects, it is
        /// possible to "merge" the collections so that new (exist in the source 
        /// collection only) and/or updated (exist in both source and destination
        /// collection but are value-wise different) objects can be copied from the 
        /// source collection to the destination collection while leaving the existing 
        /// objects in the destination collection intact. Set this value to true to 
        /// overried the default behavior and merge the source and destination items 
        /// for Lists of UberObjects and for Dictionaries.
        /// </param>
        public virtual void CopyTo(Object dest, String referenceProperties, String excludedProperties, bool mergeCollections)
        {
            _CopyTo(this, dest, TypeInfo.GetTypeInfo(this, referenceProperties, excludedProperties), mergeCollections, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of this object to another
        /// specified destination object. By default, it will not copy 
        /// references to properties that are not value types or strings. 
        /// However, any properties that you wish to copy by-value (i.e., 
        /// clone the referenced object) can be specified in the 
        /// referenceProperties parameter (see the parameter description).
        /// The copy is "smart" in the sense that it will only attempt to 
        /// copy properties that exist (and are of compatible types) 
        /// both in this object and in the  destination object and then 
        /// only if they are different (as determined by a call to the 
        /// source object's Equals method).
        /// </summary>
        /// <param name="dest">The object to which to copy the properties of this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        /// <param name="mergeCollections">
        /// By default, if they are specified as one of the referenceProperties and 
        /// if they aren't empty, collections--including arrays, Lists (standard or 
        /// generic and their derivatives), and Dictionaries (standard or generic and 
        /// their derivatives--in the destination object will be cleared and repopulated 
        /// with clones of the objects in the source collection. However, if the 
        /// collections are both Dictionaries or generic Lists of UberObjects, it is
        /// possible to "merge" the collections so that new (exist in the source 
        /// collection only) and/or updated (exist in both source and destination
        /// collection but are value-wise different) objects can be copied from the 
        /// source collection to the destination collection while leaving the existing 
        /// objects in the destination collection intact. Set this value to true to 
        /// overried the default behavior and merge the source and destination items 
        /// for Lists of UberObjects and for Dictionaries.
        /// </param>
        /// <param name="preserveGuids">
        /// By default, copying from one UberObject to another will NOT copy the
        /// UberGUID property so that the source and destination objects can be 
        /// differentiated from one another. Set this value to true to override 
        /// this behavior and copy the UberGUID from source to destination.
        /// </param>
        public virtual void CopyTo(Object dest, String referenceProperties, String excludedProperties, bool mergeCollections, bool preserveGuids)
        {
            _CopyTo(this, dest, TypeInfo.GetTypeInfo(this, referenceProperties, excludedProperties), mergeCollections, preserveGuids, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of another specified source object
        /// to this object. It will not copy references to properties that are not 
        /// value types or strings. The copy is "smart" in the sense that it will 
        /// only attempt to copy properties that exist (and are of compatible types) 
        /// both in this object and in the source object and then  only if they are 
        /// different (as determined by a call to the this object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy properties to this object.</param>
        public virtual void UpdateFrom(Object source)
        {
            _CopyTo(source, this, TypeInfo.GetTypeInfo(this, null), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of another specified source object
        /// to this object. By default, it will not copy references to properties 
        /// that are not value types or strings. However, any properties that you 
        /// wish to copy by-value (i.e., clone the referenced object) can be 
        /// specified in the referenceProperties parameter (see the parameter 
        /// description). The copy is "smart" in the sense that it will only attempt 
        /// to copy properties that exist (and are of compatible types) both in this 
        /// object and in the  destination object and then only if they are different 
        /// (as determined by a call to the this object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy properties to this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        public virtual void UpdateFrom(Object source, String referenceProperties)
        {
            _CopyTo(source, this, TypeInfo.GetTypeInfo(this, referenceProperties), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of another specified source object
        /// to this object. By default, it will not copy references to properties 
        /// that are not value types or strings. However, any properties that you 
        /// wish to copy by-value (i.e., clone the referenced object) can be 
        /// specified in the referenceProperties parameter (see the parameter 
        /// description). The copy is "smart" in the sense that it will only attempt 
        /// to copy properties that exist (and are of compatible types) both in this 
        /// object and in the  destination object and then only if they are different 
        /// (as determined by a call to the this object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy properties to this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        public virtual void UpdateFrom(Object source, String referenceProperties, String excludedProperties)
        {
            _CopyTo(source, this, TypeInfo.GetTypeInfo(this, referenceProperties, excludedProperties), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of another specified source object
        /// to this object. By default, it will not copy references to properties 
        /// that are not value types or strings. However, any properties that you 
        /// wish to copy by-value (i.e., clone the referenced object) can be 
        /// specified in the referenceProperties parameter (see the parameter 
        /// description). The copy is "smart" in the sense that it will only attempt 
        /// to copy properties that exist (and are of compatible types) both in this 
        /// object and in the  destination object and then only if they are different 
        /// (as determined by a call to the this object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy properties to this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        /// <param name="mergeCollections">
        /// By default, if they are specified as one of the referenceProperties and 
        /// if they aren't empty, collections--including arrays, Lists (standard or 
        /// generic and their derivatives), and Dictionaries (standard or generic and 
        /// their derivatives--in the destination object will be cleared and repopulated 
        /// with clones of the objects in the source collection. However, if the 
        /// collections are both Dictionaries or generic Lists of UberObjects, it is
        /// possible to "merge" the collections so that new (exist in the source 
        /// collection only) and/or updated (exist in both source and destination
        /// collection but are value-wise different) objects can be copied from the 
        /// source collection to the destination collection while leaving the existing 
        /// objects in the destination collection intact. Set this value to true to 
        /// overried the default behavior and merge the source and destination items 
        /// for Lists of UberObjects and for Dictionaries.
        /// </param>
        public virtual void UpdateFrom(Object source, String referenceProperties, String excludedProperties, bool mergeCollections)
        {
            _CopyTo(source, this, TypeInfo.GetTypeInfo(this, referenceProperties, excludedProperties), mergeCollections, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of another specified source object
        /// to this object. By default, it will not copy references to properties 
        /// that are not value types or strings. However, any properties that you 
        /// wish to copy by-value (i.e., clone the referenced object) can be 
        /// specified in the referenceProperties parameter (see the parameter 
        /// description). The copy is "smart" in the sense that it will only attempt 
        /// to copy properties that exist (and are of compatible types) both in this 
        /// object and in the  destination object and then only if they are different 
        /// (as determined by a call to the this object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy properties to this object.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        /// <param name="mergeCollections">
        /// By default, if they are specified as one of the referenceProperties and 
        /// if they aren't empty, collections--including arrays, Lists (standard or 
        /// generic and their derivatives), and Dictionaries (standard or generic and 
        /// their derivatives--in the destination object will be cleared and repopulated 
        /// with clones of the objects in the source collection. However, if the 
        /// collections are both Dictionaries or generic Lists of UberObjects, it is
        /// possible to "merge" the collections so that new (exist in the source 
        /// collection only) and/or updated (exist in both source and destination
        /// collection but are value-wise different) objects can be copied from the 
        /// source collection to the destination collection while leaving the existing 
        /// objects in the destination collection intact. Set this value to true to 
        /// overried the default behavior and merge the source and destination items 
        /// for Lists of UberObjects and for Dictionaries.
        /// </param>
        /// <param name="preserveGuids">
        /// By default, copying from one UberObject to another will NOT copy the
        /// UberGUID property so that the source and destination objects can be 
        /// differentiated from one another. Set this value to true to override 
        /// this behavior and copy the UberGUID from source to destination.
        /// </param>
        public virtual void UpdateFrom(Object source, String referenceProperties, String excludedProperties, bool mergeCollections, bool preserveGuids)
        {
            _CopyTo(source, this, TypeInfo.GetTypeInfo(this, referenceProperties, excludedProperties), mergeCollections, preserveGuids, true);
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Performs a property-by-property copy of a specified source object 
        /// to another specified destination object. It will not copy references 
        /// to properties that are not value types or strings. The copy is "smart" 
        /// in the sense that it will only attempt to copy properties that exist 
        /// (and are of compatible types) both in this object and in the 
        /// destination object and then only if they are different (as determined
        /// by a call to the source object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy.</param>
        /// <param name="dest">The object to which to copy.</param>
        public static void CopyTo(Object source, Object dest)
        {
            if (source == null || dest == null)
                throw new ArgumentNullException();
            _CopyTo(source, dest, TypeInfo.GetTypeInfo(source, null), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of a specified source object
        /// to another specified destination object. By default, it will not 
        /// copy references to properties that are not value types or strings. 
        /// However, any properties that you wish to copy by-value (i.e., 
        /// clone the referenced object) can be specified in the 
        /// referenceProperties parameter (see the parameter description).
        /// The copy is "smart" in the sense that it will only attempt to 
        /// copy properties that exist (and are of compatible types) 
        /// both in the source object and in the destination object and 
        /// then only if they are different (as determined by a call to the 
        /// source object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy.</param>
        /// <param name="dest">The object to which to copy.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        public static void CopyTo(Object source, Object dest, String referenceProperties)
        {
            if (source == null || dest == null)
                throw new ArgumentNullException();
            _CopyTo(source, dest, TypeInfo.GetTypeInfo(source, referenceProperties), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of a specified source object
        /// to another specified destination object. By default, it will not 
        /// copy references to properties that are not value types or strings. 
        /// However, any properties that you wish to copy by-value (i.e., 
        /// clone the referenced object) can be specified in the 
        /// referenceProperties parameter (see the parameter description).
        /// The copy is "smart" in the sense that it will only attempt to 
        /// copy properties that exist (and are of compatible types) 
        /// both in the source object and in the destination object and 
        /// then only if they are different (as determined by a call to the 
        /// source object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy.</param>
        /// <param name="dest">The object to which to copy.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        public static void CopyTo(Object source, Object dest, String referenceProperties, String excludedProperties)
        {
            if (source == null || dest == null)
                throw new ArgumentNullException();
            _CopyTo(source, dest, TypeInfo.GetTypeInfo(source, referenceProperties, excludedProperties), false, false, true);
        }

        /// <summary>
        /// Performs a property-by-property copy of a specified source object
        /// to another specified destination object. By default, it will not 
        /// copy references to properties that are not value types or strings. 
        /// However, any properties that you wish to copy by-value (i.e., 
        /// clone the referenced object) can be specified in the 
        /// referenceProperties parameter (see the parameter description).
        /// The copy is "smart" in the sense that it will only attempt to 
        /// copy properties that exist (and are of compatible types) 
        /// both in the source object and in the destination object and 
        /// then only if they are different (as determined by a call to the 
        /// source object's Equals method).
        /// </summary>
        /// <param name="source">The object from which to copy.</param>
        /// <param name="dest">The object to which to copy.</param>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the copying
        /// process should copy by-value. If the property is an object, the 
        /// property in the copy will contain a clone of that object instead 
        /// of a reference to the object. Similarly, if the property is a 
        /// collection, the copied object property will contain a new collection 
        /// with clones of the objects in the original collection.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can copy an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could copy the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        /// <param name="excludedProperties">
        /// The opposite of the referenceProperties parameter, you can explicitly
        /// skip certain properties when copying by specifying them as part of a 
        /// comma-separated list passed in as this parameter. Like the 
        /// referenceProperties parameter, the excludedProperties can be
        /// arbitrarily deep. The syntax to exclude the properties is identical
        /// to the syntax required to include them. Unlike the referenceProperties
        /// parameter, however, excludedProperties can include value-type properties
        /// (and strings) that you wish to exclude from the copy as well as 
        /// reference properties. Note, however, that it makes little sense to
        /// both include and exclude a reference type property since by default
        /// reference types will be excluded.
        /// </param>
        /// <param name="mergeCollections">
        /// By default, if they are specified as one of the referenceProperties and 
        /// if they aren't empty, collections--including arrays, Lists (standard or 
        /// generic and their derivatives), and Dictionaries (standard or generic and 
        /// their derivatives--in the destination object will be cleared and repopulated 
        /// with clones of the objects in the source collection. However, if the 
        /// collections are both Dictionaries or generic Lists of UberObjects, it is
        /// possible to "merge" the collections so that new (exist in the source 
        /// collection only) and/or updated (exist in both source and destination
        /// collection but are value-wise different) objects can be copied from the 
        /// source collection to the destination collection while leaving the existing 
        /// objects in the destination collection intact. Set this value to true to 
        /// overried the default behavior and merge the source and destination items 
        /// for Lists of UberObjects and for Dictionaries.
        /// </param>
        public static void CopyTo(Object source, Object dest, String referenceProperties, String excludedProperties, bool mergeCollections)
        {
            if (source == null || dest == null)
                throw new ArgumentNullException();
            _CopyTo(source, dest, TypeInfo.GetTypeInfo(source, referenceProperties, excludedProperties), mergeCollections, false, true);
        }

        #endregion

        #endregion

        #region Private _CopyTo Method

        /// <summary>
        /// Though fronted by a number of public methods, this recursive monster is the bread
        /// and butter of all Clone, CopyTo, and UpdateFrom operations.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <param name="typeInfo"></param>
        /// <param name="mergeCollections"></param>
        /// <param name="preserveGuids"></param>
        /// <param name="safeCopy"></param>
        private static void _CopyTo(Object source, Object dest, TypeInfo typeInfo, bool mergeCollections, bool preserveGuids, bool safeCopy)
        {
            // First check to see if this is a standard object or a collection
            if (!typeInfo.IsCollection)
            {
                // Okay, it's a standard object.
                // First we need to exclude the explicitly excluded properties from the copy AND
                // exclude the referenceProperties, as well, to avoid improperly copying references.
                String referenceProperties = "";
                if (typeInfo.ReferencePropertiesTypeInfo != null)
                {
                    foreach (String valueProperty in typeInfo.ReferencePropertiesTypeInfo.Keys)
                    {
                        referenceProperties += valueProperty + ",";
                    }
                }
                String excludedProperties = typeInfo.ExcludedProperties + referenceProperties;

                // Next, we start by shallow copying the object's properties
                if (safeCopy)
                {
                    MemberwiseCopyWithCheck(source, dest, excludedProperties);
                }
                else
                {
                    MemberwiseCopy(source, dest, excludedProperties);
                }

                // because UberObject Guds are copied above, we need to create new ones if we're not
                // supposed to preserve them
                if (!preserveGuids && dest is UberObject)
                {
                    ((UberObject)dest).UberGUID = Guid.NewGuid().ToString();
                }

                // If there are deep properties, retrieve them recursively
                if (typeInfo.ReferencePropertiesTypeInfo != null)
                {
                    TypeInfo destTypeInfo = typeInfo;
                    if (!source.GetType().Equals(dest.GetType()))
                    {
                        destTypeInfo = TypeInfo.GetTypeInfo(dest, referenceProperties);
                    }
                    if (destTypeInfo.ReferencePropertiesTypeInfo != null)
                    {
                        foreach (TypeInfo referencePropertyTypeInfo in typeInfo.ReferencePropertiesTypeInfo.Values)
                        {
                            String propertyName = referencePropertyTypeInfo.PropertyInfo.Name;

                            // Get the PropertyInfo object for the destination. If we can't find a 
                            // matching destination property, break out immediately
                            TypeInfo destPropertyTypeInfo;
                            if (!destTypeInfo.ReferencePropertiesTypeInfo.TryGetValue(propertyName, out destPropertyTypeInfo))
                            {
                                String message = "Could not find the property " + propertyName +
                                                 " in the destination object.";
                                if (_exceptionMode == ExceptionMode.ThrowException)
                                {
                                    throw new BadPropertyNameException(message);
                                }
                                else
                                {
                                    _OnWarning(message);
                                    continue;
                                }
                            }

                            // Retrieve the source value for the referenced property and get it's TypeInfo object.
                            Object sourceValue = referencePropertyTypeInfo.PropertyInfo.GetValue(source, null);

                            // If the source is null, just set the dest to null and move on.
                            if (sourceValue == null)
                            {
                                destPropertyTypeInfo.PropertyInfo.SetValue(dest, null, null);
                                continue;
                            }

                            // If we're creating a new object to copy over the old, then there's no reason
                            // to safely (i.e., MemberwiseCopyWithCheck) when copying to the new object, so
                            // create a variable to track this.
                            bool nestedSafeCopy = true;

                            // Pull the destination value. We'll either use it or replace it below.
                            Object destValue = destPropertyTypeInfo.PropertyInfo.GetValue(dest, null);

                            // When copying to anything but an array, we can just create a new object
                            // instance and (recursively) copy from the source to the destination.
                            if (!destPropertyTypeInfo.IsArray)
                            {
                                // If it's null, create a new instance before proceeding
                                if (destValue == null)
                                {
                                    destValue = destPropertyTypeInfo.CreateNewInstance();
                                    destPropertyTypeInfo.PropertyInfo.SetValue(dest, destValue, null);
                                    nestedSafeCopy = false;
                                }
                            }
                            else
                            {
                                // If it's an array, we need to create a NEW array instance. The 
                                // size of the new array will be either the size of the source or 
                                // if merging the size of the source plus the existing size of the dest.

                                // No matter what, though, we can't copy from something that isn't a 
                                // collection or is a dictionary.
                                if (!referencePropertyTypeInfo.IsCollection || referencePropertyTypeInfo.IsDictionary)
                                {
                                    String message = "Error copying " +
                                                     referencePropertyTypeInfo.PropertyInfo.Name + ": cannot convert " +
                                                     referencePropertyTypeInfo.Type.ToString() + " to an Array.";
                                    if (_exceptionMode == ExceptionMode.ThrowException)
                                    {
                                        throw new UnsupportedCollectionTypeException(message);
                                    }
                                    else
                                    {
                                        _OnWarning(message);
                                        continue;
                                    }
                                }

                                // Get the source collection size
                                int sourceCollectionSize = 0;
                                if (referencePropertyTypeInfo.IsArray)
                                {
                                    sourceCollectionSize = ((Array)sourceValue).Length;
                                }
                                else if (referencePropertyTypeInfo.IsList)
                                {
                                    sourceCollectionSize = ((IList)sourceValue).Count;
                                }
                                else
                                {
                                    // Dang. We need to enumerate the collection
                                    IEnumerator sourceEnumerator = ((IEnumerable)sourceValue).GetEnumerator();
                                    while (sourceEnumerator.MoveNext())
                                        sourceCollectionSize++;
                                }

                                // If merging, the new array size should be the old + the source
                                if (mergeCollections)
                                {
                                    sourceCollectionSize += ((Array)destValue).Length;
                                }

                                // Create the new destination array.
                                Array newDestArray = Array.CreateInstance(destPropertyTypeInfo.ElementType, sourceCollectionSize);

                                // If merging, copy the old destination array to the first elements of 
                                // the new destination array. No need to clone these since they were
                                // already in the source (and we'd want to keep references to existing
                                // reference types anyway).
                                if (mergeCollections)
                                {
                                    int currentPosition = 0;
                                    foreach (Object destElement in (Array)destValue)
                                    {
                                        newDestArray.SetValue(destElement, currentPosition++);
                                    }
                                }

                                // Finally, set the property to the new destination array
                                destValue = newDestArray;
                                destPropertyTypeInfo.PropertyInfo.SetValue(dest, destValue, null);
                                nestedSafeCopy = false;
                            }

                            _CopyTo(sourceValue, destValue, referencePropertyTypeInfo, mergeCollections, preserveGuids, nestedSafeCopy);
                        }
                    }
                }
            }
            else // IS A COLLECTION
            {
                TypeInfo destTypeInfo = TypeInfo.GetTypeInfo(dest, null);

                // We can quickly exit with an exception if the source and dest element types
                // are incompatible.
                if (!destTypeInfo.IsCollection)
                {
                    String message = "Error copying " + typeInfo.PropertyInfo.Name + 
                                     ": cannot copy from a collection to a non-collection.";
                    if (_exceptionMode == ExceptionMode.ThrowException)
                    {
                        throw new UnsupportedCollectionTypeException(message);
                    }
                    else
                    {
                        _OnWarning(message);
                        return;
                    }
                }

                if (typeInfo.IsDictionary != destTypeInfo.IsDictionary)
                {
                    String message = "Error copying " + typeInfo.PropertyInfo.Name +
                                     ": cannot convert dictionaries to other collection types.";
                    if (_exceptionMode == ExceptionMode.ThrowException)
                    {
                        throw new UnsupportedCollectionTypeException(message);
                    }
                    else
                    {
                        _OnWarning(message);
                        return;
                    }
                }

                if (typeInfo.IsValueType && !destTypeInfo.ElementType.IsAssignableFrom(typeInfo.ElementType))
                {
                    String message = "Error copying " + typeInfo.PropertyInfo.Name +
                                     ". Incompatible collection types: " + typeInfo.Type.ToString() +
                                     " and " + destTypeInfo.Type.ToString();
                    if (_exceptionMode == ExceptionMode.ThrowException)
                    {
                        throw new UnsupportedCollectionTypeException(message);
                    }
                    else
                    {
                        _OnWarning(message);
                        return;
                    }
                }

                // If the destination is a list and we're dealing with UberObjects, we can do some 
                // fancy work to merge the lists togtether if requested. First we need the TypeInfo 
                // for the dest as well as the (passed) source.
                if (destTypeInfo.IsList &&
                    typeof(UberObject).IsAssignableFrom(typeInfo.ElementType) &&
                    typeof(UberObject).IsAssignableFrom(destTypeInfo.ElementType))
                {
                    // If they're UberObjects, then we can assume the presence of an UberGUID, which
                    // means that we can synchronize and update rather than destroy and refill.
                    // Cool, so lets start by determining which items are in both source and
                    // dest using LINQ.
                    IEnumerable<UberObject> sourceEnumerable = ((IEnumerable)source).Cast<UberObject>();
                    IEnumerable<UberObject> destEnumerable = ((IEnumerable)dest).Cast<UberObject>();
                    IList destList = dest as IList;
                    
                    var commonItems = from sourceItem in sourceEnumerable
                                      join destItem in destEnumerable
                                           on sourceItem.UberGUID equals destItem.UberGUID
                                      select new { sourceItem, destItem };

                    // Okay, updated. Now lets determine what is in the source and NOT in the dest and
                    // add a clone to the dest.
                    // NOTE: We "pre-execute" these two queries (by converting them ToList) to avoid
                    //       some convoluted issues that might arise by querying AFTER updates, inserts,
                    //       and deletes.
                    List<UberObject> sourceOnly = (from sourceItem in sourceEnumerable
                                                   join destItem in destEnumerable
                                                        on sourceItem.UberGUID equals destItem.UberGUID into tempItems
                                                   from destItem in tempItems.DefaultIfEmpty()
                                                   where destItem == null
                                                   select sourceItem).ToList<UberObject>();

                    // Finally, IF we're not to mergeCollections (parameter), lets determine what is ONLY
                    // in the dest and remove it from the collection.
                    List<UberObject> destOnly = null;
                    if (!mergeCollections)
                    {
                        destOnly = (from destItem in destEnumerable
                                    join sourceItem in sourceEnumerable
                                         on destItem.UberGUID equals sourceItem.UberGUID into tempItems
                                    from sourceItem in tempItems.DefaultIfEmpty()
                                    where sourceItem == null
                                    select destItem).ToList<UberObject>();
                    }

                    // Now we process the updates, inserts, and (optionally) deletes
                    foreach (var commonItem in commonItems)
                    {
                        _CopyTo(commonItem.sourceItem, commonItem.destItem, typeInfo.ElementTypeInfo, mergeCollections, preserveGuids, true);
                    }

                    foreach (UberObject sourceItem in sourceOnly)
                    {
                        UberObject destItem = (UberObject)CreateObjectInstance(destTypeInfo.ElementType);
                        _CopyTo(sourceItem, destItem, typeInfo.ElementTypeInfo, mergeCollections, preserveGuids, true);
                        destList.Add(destItem);
                    }

                    if (!mergeCollections)
                    {
                        foreach (UberObject destItem in destOnly) // **** NOT NEEDED??? .ToList<UberObject>())
                        {
                            destList.Remove(destItem);
                        }
                    }
                }
                else // Not a List of UberObjects
                {
                    // Otherwise, we just have to clear and repopulate the lists--too bad C# doesn't 
                    // provide a bit more elegance by allowing generic collections to be down cast
                    // (e.g., List<UberObject> to List<Object>). This would have provided me with
                    // much more flexibility. As it stands I can really ONLY down cast List<> to IList
                    // and Dictionary<> to IDictionary. Oh well...

                    if (typeInfo.IsDictionary)
                    {
                        // Dictionaries are a special case because we have to handle both the keys and the 
                        // values. To do this, we'll loop through the keys from the source dictionary,
                        // clone each value, and add it to the destination dictionary.
                        IDictionary sourceDict = source as IDictionary;
                        IDictionary destDict = dest as IDictionary;

                        // So we start by OPTIONALLY clearing the destination.
                        if (!mergeCollections)
                            destDict.Clear();

                        // Now we loop through the source keys and add the values to the destination. Note,
                        // we are using the Item notation rather than the Add method because this will
                        // replace existing entries rather than throwing an error (at least according to the
                        // documentation). This way we achieve a quasi-merge, if requested.
                        foreach (Object key in sourceDict.Keys)
                        {
                            if (typeInfo.ElementTypeInfo.IsValueType)
                            {
                                destDict[key] = sourceDict[key];
                            }
                            else
                            {
                                object sourceItem = sourceDict[key];
                                object destItem = CreateObjectInstance(destTypeInfo.ElementType);
                                _CopyTo(sourceItem, destItem, typeInfo.ElementTypeInfo, mergeCollections, preserveGuids, true);
                                destDict[key] = destItem;
                            }
                        }
                    }
                    else
                    {
                        // The source can be any enumerable, but the dest must be a known type of collection
                        // so that we can clear and fill it. If we're here, the source is known to be
                        // enumerable. Just check to make sure the dest is supported.
                        if (!destTypeInfo.IsList && !destTypeInfo.IsArray && !destTypeInfo.IsListSource)
                        {
                            String message = "Error copying " + typeInfo.PropertyInfo.Name +
                                             ". Unsupported destination collection type: " +
                                             destTypeInfo.Type.ToString();
                            if (_exceptionMode == ExceptionMode.ThrowException)
                            {
                                throw new UnsupportedCollectionTypeException(message);
                            }
                            else
                            {
                                _OnWarning(message);
                                return;
                            }
                        }

                        // Create an enumerator to allow us to zip through the source regardless of type.
                        IEnumerator sourceEnumerator = ((IEnumerable)source).GetEnumerator();

                        // if merging to an array, we need to figure out where to start. This will be
                        // at the end of the existing values. But since the new array is already sized,
                        // we need to calculate this by subtracting the source collection size from the 
                        // new dest array size.
                        int startingPos = 0;
                        if (mergeCollections && destTypeInfo.IsArray)
                        {
                            int sourceCollectionSize = 0;
                            if (typeInfo.IsArray)
                            {
                                sourceCollectionSize = ((Array)source).Length;
                            }
                            else if (typeInfo.IsList)
                            {
                                sourceCollectionSize = ((IList)source).Count;
                            }
                            else
                            {
                                // Dang. We need to enumerate the collection
                                while (sourceEnumerator.MoveNext())
                                    sourceCollectionSize++;
                                sourceEnumerator.Reset();
                            }
                            startingPos = ((Array)dest).Length - sourceCollectionSize - 1;
                        }
                        
                        // If the dest is a list or list source and mergeCollections is false, we need to clear it
                        if ((destTypeInfo.IsList || destTypeInfo.IsListSource) && !mergeCollections)
                            AsList(dest).Clear();

                        // Now we can just enumerate the source and copy the values to the dest
                        // Now get an enumerator to use to copy the source values to the dest.
                        int currentPosition = startingPos;
                        while (sourceEnumerator.MoveNext())
                        {
                            Object sourceItem = sourceEnumerator.Current;
                            Object destItem;
                            if (typeInfo.ElementTypeInfo.IsValueType)
                            {
                                destItem = sourceItem;
                            }
                            else
                            {
                                destItem = CreateObjectInstance(destTypeInfo.ElementType);
                                _CopyTo(sourceItem, destItem, typeInfo.ElementTypeInfo, mergeCollections, preserveGuids, true);
                            }

                            if (destTypeInfo.IsArray)
                            {
                                ((Array)dest).SetValue(destItem, currentPosition++);
                            }
                            else
                            {
                                AsList(dest).Add(destItem);
                            }
                        }
                    }

                }

            }

        }

        #endregion

        #region Change Tracking Methods

        private TypeInfo _trackedTypeInfo;
        private Stack<UberObject> _bookmarks;

        /// <summary>
        /// <para>
        /// Initializes the change tracking process for an instance of an UberObject. Must be
        /// called in order to set or clear bookmarks, to undo changes, or to determine if the
        /// object is "dirty." Calling this method does incur some overhead in terms of memory
        /// footprint by adding internal clone(s) to allow for undoing changes. This overhead 
        /// can be removed by a subsequent call to StopTrackingChanges().
        /// </para>
        /// <para>
        /// By default, changes are tracked only in the base object (and then only to the 
        /// value-types and strings). If you wish to track changes to referenced objects or object 
        /// collections, use BeginTrackingChanges(string).
        /// </para>
        /// </summary>
        public virtual void BeginTrackingChanges()
        {
            if (IsTrackingChanges) throw new AlreadyTrackingChangesException();

            BeginTrackingChanges(null);
        }

        /// <summary>
        /// <para>
        /// Initializes the change tracking process for an instance of an UberObject. Must be
        /// called in order to set or clear bookmarks, to undo changes, or to determine if the
        /// object is "dirty." Calling this method does incur some overhead in terms of memory
        /// footprint by adding internal clone(s) to allow for rollback. This overhead can be 
        /// removed by a subsequent call to StopTrackingChanges().
        /// </para>
        /// <para>
        /// By default, changes are tracked only in the base object. This version of the method
        /// allows you to track changes to referenced objects or object collections by explicitly
        /// specifying which referenced objects/collections you wish to track. See the parameter
        /// description for "referenceProperties" for a full description of how to configure this
        /// functionality.
        /// </para>
        /// </summary>
        /// <param name="referenceProperties">
        /// <para>
        /// A comma-separated string of reference properties that the tracking process track. 
        /// If the property is an object, any changes to the object will also be tracked. 
        /// Similarly, if the property is a collection, changes to the collection itself AS 
        /// WELL AS to the objects in the collection will be tracked.
        /// </para>
        /// <para>
        /// Properties may be n-deep as well, meaning that you can track an
        /// object contained within an object contained within an object. This
        /// applies to objects within objects within collected objects, as well.
        /// You simply need to specify the deep properties using standard
        /// object-dot-object notation.
        /// </para>
        /// <para>
        /// For example, using a standard order entry system with 
        /// Customers=>Orders=>OrderDetails=>Products sample hierarchy, you
        /// could track the entire hierarchy using the by passing 
        /// "Orders,Orders.OrderDetails,Orders.OrderDetails.Product"
        /// as your referenceProperties parameter.
        /// </para>
        /// <para>
        /// Note that any errors in the notation of this string will not
        /// be caught at design time but will instead result in debug
        /// warnings. Watch for these warnings in the output console.
        /// </para>
        /// </param>
        public virtual void BeginTrackingChanges(String referenceProperties)
        {
            if (IsTrackingChanges) throw new AlreadyTrackingChangesException();

            _trackedTypeInfo = TypeInfo.GetTypeInfo(this, referenceProperties);
            OriginalObject = (UberObject)this._Clone(_trackedTypeInfo, true);

            IsTrackingChanges = true;
        }

        /// <summary>
        /// <para>
        /// Create a "bookmark" (checkpoint, rollback point, etc.)--a clone of the current state of
        /// a tracked object (and all of its tracked "referenceProperties")--to which the UberObject's
        /// state can be reverted by calling the UndoChanges() method.
        /// </para>
        /// </summary>
        public virtual void CreateBookmark()
        {
            if (!IsTrackingChanges) throw new NotTrackingChangesException();

            if (_bookmarks == null) _bookmarks = new Stack<UberObject>();
            _bookmarks.Push((UberObject)this._Clone(_trackedTypeInfo, true));
        }

        /// <summary>
        /// Reverts a tracked UberObject back to a prior state. If there are no bookmarks, the 
        /// object is reverted to its original state. Otherwise it is reverted back to its state
        /// at the time of the most recent bookmark.
        /// </summary>
        public virtual void UndoChanges()
        {
            if (!IsTrackingChanges) throw new NotTrackingChangesException();

            if (_bookmarks != null && _bookmarks.Count > 0)
            {
                _CopyTo(_bookmarks.Pop(), this, _trackedTypeInfo, false, true, true);
                _bookmarks.TrimExcess();
            }
            else
            {
                _CopyTo(this.OriginalObject, this, _trackedTypeInfo, false, true, true);
            }
        }

        /// <summary>
        /// Removes the most recently created bookmark, but leaves the UberObject in its current state.
        /// This frees the resources allocated to the bookmark clone.
        /// </summary>
        public virtual void ClearBookmark()
        {
            if (!IsTrackingChanges) throw new NotTrackingChangesException();

            if (_bookmarks != null && _bookmarks.Count > 0)
            {
                _bookmarks.Pop();
                _bookmarks.TrimExcess();
            }
        }

        /// <summary>
        /// Reverts a tracked UberObject back to it original state.
        /// </summary>
        public virtual void UndoAllChanges()
        {
            if (!IsTrackingChanges) throw new NotTrackingChangesException();

            ClearAllBookmarks();
            UndoChanges();
        }

        /// <summary>
        /// Frees the resources allocated by the creation of bookmark clones.
        /// </summary>
        public virtual void ClearAllBookmarks()
        {
            if (!IsTrackingChanges) throw new NotTrackingChangesException();

            _bookmarks = null;
        }

        /// <summary>
        /// Frees resources associated with change tracking. By default, the clone of the original object that was
        /// created when BeginChangeTracking() was called is destroyed. Use StopTrackingChanges(bool) to override
        /// this behavior if you later wish to refernce the original values.
        /// </summary>
        public virtual void StopTrackingChanges()
        {
            if (!IsTrackingChanges) throw new NotTrackingChangesException();

            StopTrackingChanges(false);
        }

        /// <summary>
        /// Frees resources associated with change tracking. By default, the clone of the original object that was
        /// created when BeginChangeTracking() was called is destroyed. However, passing true to this method will
        /// retain the original object clone for later reference.
        /// </summary>
        /// <param name="keepOriginal">If true, retain the clone of the original object.</param>
        public virtual void StopTrackingChanges(bool keepOriginal)
        {
            if (!IsTrackingChanges) throw new NotTrackingChangesException();

            // First, call the _Equals method to get the final "IsDirty" state of the object before we
            // completely remove everything we need to do so.
            IsDirty = _Equals(this, OriginalObject, _trackedTypeInfo);
            
            // Delete change tracking resources.
            ClearAllBookmarks();
            _trackedTypeInfo = null;
            if (!keepOriginal) OriginalObject = null;

            IsTrackingChanges = false;
        }

        #endregion

        #region TypeInfo Class

        // Hold a list of generated TypeInfo objects so that we don't have to generate them
        // each time that they are used.
        private static Dictionary<String, TypeInfo> _objectInfoCache = new Dictionary<string, TypeInfo>();

        private class TypeInfo
        {
            public Type Type { get; private set; }
            public Type ElementType { get; private set; }
            public bool IsValueType { get; private set; }
            public bool IsCollection { get; private set; }
            public bool IsArray { get; private set; }
            public bool IsList { get; private set; }
            public bool IsListSource { get; private set; }
            public bool IsDictionary { get; private set; }

            public String ReferenceProperties { get; private set; }
            public String ExcludedProperties { get; private set; }

            public PropertyInfo PropertyInfo { get; private set; }

            public TypeInfo ParentTypeInfo { get; private set; }
            public TypeInfo ElementTypeInfo { get; private set; }
            public Dictionary<String, TypeInfo> ReferencePropertiesTypeInfo { get; private set; }

            #region Public Methods (GetTypeInfo)

            public static TypeInfo GetTypeInfo(Object rootObject, String referenceProperties)
            {
                if (rootObject == null) return null;
                return GetTypeInfo(rootObject.GetType(), referenceProperties, null);
            }

            public static TypeInfo GetTypeInfo(Type type, String referenceProperties)
            {
                return GetTypeInfo(type, referenceProperties, null);
            }

            public static TypeInfo GetTypeInfo(Object rootObject, String referenceProperties, String excludedProperties)
            {
                if (rootObject == null) return null;
                return GetTypeInfo(rootObject.GetType(), referenceProperties, excludedProperties);
            }

            public static TypeInfo GetTypeInfo(Type type, String referenceProperties, String excludedProperties)
            {
                TypeInfo typeInfo;

                if (!String.IsNullOrWhiteSpace(referenceProperties)) referenceProperties = referenceProperties.Trim();
                if (!String.IsNullOrWhiteSpace(excludedProperties)) excludedProperties = excludedProperties.Trim();

                // First, lets do a quick check against a static cache to see if we already have an TypeInfo object 
                // for this class + string combination. Note that this isn't flawless since the deep properties string can
                // really be in any order, but it's far better than nothing.
                String cacheKey = type.FullName + referenceProperties + excludedProperties;
                if (_objectInfoCache.TryGetValue(cacheKey, out typeInfo))
                    return typeInfo;

                // Split the raw comma separated lists into specific (potentially deep themselves) object
                // names (property names really).
                List<String> referencePropertiesList = null, excludedPropertiesList = null;
                if (referenceProperties != null) referencePropertiesList = referenceProperties.Split(',').ToList<String>();
                if (excludedProperties != null) excludedPropertiesList = excludedProperties.Split(',').ToList<String>();

                // Call the internal, recursive method to fill up the structure
                typeInfo = _GetTypeInfo(type, referencePropertiesList, excludedPropertiesList);

                // Lastly, add it to the cache so we don't have to go through all this again
                _objectInfoCache[cacheKey] = typeInfo;

                return typeInfo;
            }

            public Object CreateNewInstance()
            {
                return CreateObjectInstance(this.Type);
            }

            public Object CreateNewElementInstance()
            {
                return CreateObjectInstance(this.ElementType);
            }

            #endregion

            #region Private Methods

            private static TypeInfo _GetTypeInfo(Type objectType, List<String> referencePropertiesList, List<String> excludedPropertiesList)
            {

                TypeInfo typeInfo;

                // Okay, not in cache, so let's move forward
                // We treat the root object specially because it is an actual object and not
                // a hypothetical PropertyInfo object. Thus, let's go ahead and fill in what
                // we can about the root object.
                typeInfo = new TypeInfo();
                typeInfo.Type = objectType;
                typeInfo.ParentTypeInfo = null;
                typeInfo.IsValueType = CheckIsValueType(typeInfo.Type);
                typeInfo.IsArray = CheckIsArray(typeInfo.Type);
                typeInfo.IsCollection = CheckIsCollection(typeInfo.Type);
                typeInfo.IsDictionary = CheckIsDictionary(typeInfo.Type);
                typeInfo.IsList = CheckIsList(typeInfo.Type);
                typeInfo.IsListSource = CheckIsListSource(typeInfo.Type);

                if (typeInfo.IsArray || typeInfo.IsCollection)
                {
                    // Arrays have element types. Collections have "generic arguments" instead.
                    if (typeInfo.IsArray)
                    {
                        typeInfo.ElementType = typeInfo.Type.GetElementType();
                    }
                    else if (typeInfo.IsCollection)
                    {
                        // Need to determine if this is a derived list/dictionary. If so, we need to
                        // get the generic arguments from the base type.
                        Type genericType = typeInfo.Type;
                        if (!genericType.IsGenericType)
                        {
                            genericType = genericType.BaseType;
                            // Unlikely, I think, but just in case.
                            if (!genericType.IsGenericType)
                            {
                                Trace.TraceWarning("Unsupported collection type: " + typeInfo.Type.FullName);
                                return null;
                            }
                        }

                        // Dictionaries carry their element type in the second generic argument (i.e.,
                        // Dictionary<KeyType,ElementType>). Lists have only one generic argument, which
                        // thankfully is the element type (i.e., List<ElementType>).
                        if (typeInfo.IsDictionary)
                        {
                            typeInfo.ElementType = genericType.GetGenericArguments()[1];
                        }
                        else
                        {
                            typeInfo.ElementType = genericType.GetGenericArguments()[0];
                        }
                    }

                    // Recursively get TypeInfo for the ElementType (and its Parent).
                    typeInfo.ElementTypeInfo = _GetTypeInfo(typeInfo.ElementType, referencePropertiesList, excludedPropertiesList);
                    typeInfo.ElementTypeInfo.ParentTypeInfo = typeInfo.ParentTypeInfo;
                }
                else
                {
                    typeInfo.ElementType = null;
                    typeInfo.ElementTypeInfo = null;
                }

                // Now we need to (optionally) get the nested objects and set the excluded objects.
                if (referencePropertiesList == null) referencePropertiesList = new List<string>();
                if (excludedPropertiesList == null) excludedPropertiesList = new List<string>();

                // Now we need to process those pesky nested objects. Uugh!!!
                // First, break down the referencePropertiesList into a dictionary of lists. We want each base property
                // name--i.e., the stuff before the first period or the full thing if their is no period--as the
                // key, and we want a list of the remainders--i.e., the nested objects--as the value.
                //
                // e.g., "propA.propB.propC" becomes "propA" => {"propB.propC"}
                //
                // Note that this is necessarily additive, such that we don't overwrite previously stated nested properties.
                //
                // e.g., "propA.propB, propA.propC" becomse "propA" => {"propB", "propC"}
                //
                Dictionary<String, List<String>> referencePropertiesDictionary = new Dictionary<string, List<string>>();
                foreach (String referencePropertiesString in referencePropertiesList)
                {
                    if (String.IsNullOrWhiteSpace(referencePropertiesString))
                        continue;

                    List<String> splitProperties = referencePropertiesString.Trim().Split('.').ToList();
                    String propertyName = splitProperties[0];
                    splitProperties.RemoveAt(0);

                    List<String> propertiesList;
                    if (!referencePropertiesDictionary.TryGetValue(propertyName, out propertiesList))
                    {
                        propertiesList = new List<string>();
                        referencePropertiesDictionary.Add(propertyName, propertiesList);
                    }

                    if (splitProperties.Count > 0)
                        propertiesList.Add(String.Join(".", splitProperties));
                }

                // Now do the exact same thing for excluded properties
                Dictionary<String, List<String>> excludedPropertiesDictionary = new Dictionary<string, List<string>>();
                foreach (String excludedPropertiesString in excludedPropertiesList)
                {
                    if (String.IsNullOrWhiteSpace(excludedPropertiesString))
                        continue;

                    List<String> splitProperties = excludedPropertiesString.Trim().Split('.').ToList();
                    String propertyName = splitProperties[0];
                    splitProperties.RemoveAt(0);

                    List<String> propertiesList;
                    if (!excludedPropertiesDictionary.TryGetValue(propertyName, out propertiesList))
                    {
                        propertiesList = new List<string>();
                        excludedPropertiesDictionary.Add(propertyName, propertiesList);
                    }

                    if (splitProperties.Count > 0)
                        propertiesList.Add(String.Join(".", splitProperties));
                }

                // Now, all we have to do is loop through each object (property) name, get our info,
                // and then recursively call this method.
                foreach (String referencePropertyName in referencePropertiesDictionary.Keys)
                {
                    // Since we're now dealing with properties instead of actual objects. So, we need
                    // to get the PropertyInfo structure for this property.
                    PropertyInfo referencePropertyInfo = typeInfo.Type.GetProperty(referencePropertyName);
                    if (referencePropertyInfo == null)
                    {
                        String message = "Could not find the property " + referencePropertyName +
                                            " in the destination object.";
                        if (_exceptionMode == ExceptionMode.ThrowException)
                        {
                            throw new BadPropertyNameException(message);
                        }
                        else
                        {
                            _OnWarning(message);
                            continue;
                        }
                    }

                    // FINALLY, we can now recursively call this method to add TypeInfo objects
                    // to the referencePropertiesTypeInfo dictionary
                    Type referencePropertyType = referencePropertyInfo.PropertyType;
                    List<String> nestedReferencePropertiesList = referencePropertiesDictionary[referencePropertyName];
                    List<String> nestedExcludedPropertiesList;
                    excludedPropertiesDictionary.TryGetValue(referencePropertyName, out nestedExcludedPropertiesList);

                    TypeInfo valueTypeInfo = _GetTypeInfo(referencePropertyType, nestedReferencePropertiesList, nestedExcludedPropertiesList);
                    if (valueTypeInfo == null) continue;

                    valueTypeInfo.PropertyInfo = referencePropertyInfo;
                    valueTypeInfo.ParentTypeInfo = typeInfo;

                    if (typeInfo.ReferencePropertiesTypeInfo == null) typeInfo.ReferencePropertiesTypeInfo = new Dictionary<string, TypeInfo>();
                    typeInfo.ReferencePropertiesTypeInfo.Add(referencePropertyName, valueTypeInfo);
                }

                // Now we must separate the real excluded properties from those that simply were used as a prefix to get
                // to a nested reference property. To do this, we'll loop through the list of referenceProperties and remove
                // any matches from the excluded properties dictionary unless the reference property of the dictionary is
                // null (which would indicate that the user actually wishes to exclude the reference property (this shouldn't
                // happen unless the user makes a syntactical mistake, actually).
                foreach (String valuePropertyName in referencePropertiesDictionary.Keys)
                {
                    List<string> nestedExcludedPropertiesList = null;
                    excludedPropertiesDictionary.TryGetValue(valuePropertyName, out nestedExcludedPropertiesList);
                    if (nestedExcludedPropertiesList != null)
                    {
                        // if their are no entries in the list, then the user probably actually intended to exclude this
                        // even though it was included as a reference property. Otherwise, remove the keyed entry from the list.
                        if (nestedExcludedPropertiesList.Count != 0)
                        {
                            excludedPropertiesDictionary.Remove(valuePropertyName);
                        }
                    }
                }

                // Now we can set the ReferenceProperties and ExcludedProperties strings to a comma separated 
                // concatenation of the values in this list.
                typeInfo.ReferenceProperties = String.Join(",", referencePropertiesDictionary.Keys);
                typeInfo.ExcludedProperties = String.Join(",", excludedPropertiesDictionary.Keys);

                return typeInfo;
            }

            #endregion

        }

        #endregion

        #region MatchedProperty Class / Methods

        /// <summary>
        /// The MatchedProperty class simply encapsulates the attributes of a pair of
        /// properties from two classes that are deemed the "same" property.
        /// </summary>
        public class MatchedProperty
        {
            /// <summary>
            /// Read Only. The first matched property.
            /// </summary>
            public PropertyInfo FirstProperty { get; private set; }

            /// <summary>
            /// Read Only. The second matched property.
            /// </summary>
            public PropertyInfo SecondProperty { get; private set; }

            /// <summary>
            /// Read Only. Assuming that FirstProperty is the source and SecondProperty is
            /// the destination, is the first property assignable to the second property?
            /// (i.e., can this property be copied?)
            /// </summary>
            public bool AreAssignable { get; private set; }

            /// <summary>
            /// Read Only. If false, both properties are value types. Otherwise, at least 
            /// one is a reference type.
            /// </summary>
            public bool HasReferenceType { get; private set; }

            /// <summary>
            /// Read Only. If false, both properties are non-nullable. Otherwise, at least 
            /// one is a nullable value type.
            /// </summary>
            public bool HasNullableType { get; private set; }

            /// <summary>
            /// The sole constructor for a matched property.
            /// </summary>
            /// <param name="firstProperty">The PropertyInfo for the first of the matched pair.</param>
            /// <param name="secondProperty">The PropertyInfo for the second of the matched pair.</param>
            public MatchedProperty(PropertyInfo firstProperty, PropertyInfo secondProperty)
            {
                FirstProperty = firstProperty;
                SecondProperty = secondProperty;
                AreAssignable = SecondProperty.GetNonNullableType().IsAssignableFrom(SecondProperty.GetNonNullableType());
                HasReferenceType = !FirstProperty.PropertyType.IsValueType || !SecondProperty.PropertyType.IsValueType;
                HasNullableType = FirstProperty.IsNullable() || SecondProperty.IsNullable();
            }

        }

        /// <summary>
        /// Passing in two object types and a string of excluded properties, this method loops
        /// through the properties in each type trying to find matched pairs. A matched pair
        /// must have the same property name and "compatible" property types.
        /// </summary>
        /// <param name="firstType">The type of the first object to match.</param>
        /// <param name="secondType">The type of the second object to match.</param>
        /// <param name="excludedProperties">
        /// A comma-separated list of properties to exclude from the list of matches.
        /// </param>
        /// <returns></returns>
        public static List<MatchedProperty> GetMatchedProperties(Type firstType, Type secondType, String excludedProperties)
        {
            List<MatchedProperty> matchedProperties = new List<MatchedProperty>();

            // Get a list of excluded properties
            List<String> excludedPropertiesList = new List<string>();
            if (excludedProperties != null)
            {
                List<String> tempExcludedPropertiesList = excludedProperties.Split(',').ToList<String>();
                foreach (String excludedProperty in tempExcludedPropertiesList)
                {
                    String trimmedExcludedProperty = excludedProperty.Trim();
                    if (trimmedExcludedProperty.Length > 0) excludedPropertiesList.Add(trimmedExcludedProperty);
                }
            }

            PropertyInfo[] firstProperties = firstType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo firstProperty in firstProperties)
            {
                // Skip excluded properties
                if (excludedPropertiesList.Contains(firstProperty.Name))
                    continue;

                // Skip properties that are not value types (or strings)
                if (!CheckIsValueType(firstProperty.PropertyType))
                    continue;

                // Skip properties that don't exist in the second object
                PropertyInfo secondProperty = secondType.GetProperty(firstProperty.Name);
                if (secondProperty == null)
                    continue;

                // Skip properties that are incompatible (though nullable -> non-nullables are incompatible,
                // don't skip these--instead when assigning we'll assign them GetValueOrDefault();
                if ( !firstProperty.GetNonNullableType().IsAssignableFrom(secondProperty.GetNonNullableType()) &&
                     !secondProperty.GetNonNullableType().IsAssignableFrom(firstProperty.GetNonNullableType()) )
                    continue;

                matchedProperties.Add(new MatchedProperty(firstProperty, secondProperty));
            }

            return matchedProperties;
        }
        
        #endregion

        #region MemberwiseCopy Methods and Dynmic Method Generator

        // Hold a list of generated IMemberwiseCopy objects so that we don't have to generate them
        // each time that they are used.
        private static Dictionary<string, MemberwiseCopyDelegate> _memberwiseCopyCache = new Dictionary<string, MemberwiseCopyDelegate>();
        private static Dictionary<string, MemberwiseCopyWithCheckDelegate> _memberwiseCopyWithCheckCache = new Dictionary<string, MemberwiseCopyWithCheckDelegate>();

        #region Public MemberwiseCopy Methods

        /// <summary>
        /// Copy the property values from one object to another. Equivalent to
        /// <see cref="CopyObject(object,object)"/>.
        /// Note source and destination object types need not be the same. Only the properties
        /// that are common between the two types will be copied.
        /// </summary>
        /// <param name="sourceObject">Object from which to copy.</param>
        /// <param name="destinationObject">Object to copy property values to.</param>
        public static void MemberwiseCopy(object sourceObject, object destinationObject)
        {
            if (sourceObject == null || destinationObject == null)
                throw new NullReferenceException("Cannot copy to or from a null object");
            
            try
            {
                MemberwiseCopy(sourceObject, destinationObject, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Copy the property values from one object to another.
        /// Note source and destination object types need not be the same. Only the properties
        /// that are common between the two types will be copied.
        /// </summary>
        /// <param name="sourceObject">Object from which to copy.</param>
        /// <param name="destinationObject">Object to copy property values to.</param>
        /// <param name="excludedProperties">A comma-separated string of property names that should be excluded
        /// from the copying process.</param>
        public static void MemberwiseCopy(object sourceObject, object destinationObject, String excludedProperties)
        {
            if (sourceObject == null || destinationObject == null)
                throw new NullReferenceException("Cannot copy to or from a null object");
            
            try
            {
                MemberwiseCopyDelegate memberwiseCopyMethod = _GetMemberwiseCopyDelegate(sourceObject.GetType(), destinationObject.GetType(), excludedProperties);
                memberwiseCopyMethod(sourceObject, destinationObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Copy the property values from one object to another. Only copies values that differ
        /// between source and destination. Slightly slower in general, but safer and perhaps more
        /// efficient for objects that implement change notification.
        /// </summary>
        /// <param name="sourceObject">Object from which to copy.</param>
        /// <param name="destinationObject">Object to copy property values to.</param>
        public static void MemberwiseCopyWithCheck(object sourceObject, object destinationObject)
        {
            if (sourceObject == null || destinationObject == null)
                throw new NullReferenceException("Cannot copy to or from a null object");
            
            try
            {
                MemberwiseCopyWithCheck(sourceObject, destinationObject, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Copy the property values from one object to another. Only copies values that differ
        /// between source and destination. Slightly slower in general, but safer and perhaps more
        /// efficient for objects that implement change notification.
        /// </summary>
        /// <param name="sourceObject">Object from which to copy.</param>
        /// <param name="destinationObject">Object to copy property values to.</param>
        /// <param name="excludedProperties">A comma-separated string of property names that should be excluded
        /// from the copying process.</param>
        public static void MemberwiseCopyWithCheck(object sourceObject, object destinationObject, String excludedProperties)
        {
            if (sourceObject == null || destinationObject == null)
                throw new NullReferenceException("Cannot copy to or from a null object");
            
            try
            {
                MemberwiseCopyWithCheckDelegate memberwiseCopyWithCheckMethod = _GetMemberwiseCopyWithCheckDelegate(sourceObject.GetType(), destinationObject.GetType(), excludedProperties);
                memberwiseCopyWithCheckMethod(sourceObject, destinationObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private MemberwiseCopy Methods

        // Delegates used to define the basic contract for all dynamically created
        // "MemberwiseCopy" objects. Had to be defined as public in order for the
        // dynamic assemblies to be able to see them, however, they not at all useful
        // outside of this class. INTERNAL USE ONLY
        private delegate void MemberwiseCopyDelegate(object sourceObject, object destinationObject);
        private delegate void MemberwiseCopyWithCheckDelegate(object sourceObject, object destinationObject);

        private static MemberwiseCopyDelegate _GetMemberwiseCopyDelegate(Type sourceType, Type destinationType, String excludedProperties)
        {
            String key = sourceType.FullName + destinationType.FullName + excludedProperties;

            MemberwiseCopyDelegate memberwiseCopyDelegate;

            if (!_memberwiseCopyCache.TryGetValue(key, out memberwiseCopyDelegate))
            {
                memberwiseCopyDelegate = _GenerateMemberwiseCopyDelegate(sourceType, destinationType, excludedProperties);
                _memberwiseCopyCache[key] = memberwiseCopyDelegate;
            }

            if (memberwiseCopyDelegate == null)
                throw new NoCompatibleTypesException();
            
            return memberwiseCopyDelegate;
        }

        private static MemberwiseCopyWithCheckDelegate _GetMemberwiseCopyWithCheckDelegate(Type sourceType, Type destinationType, String excludedProperties)
        {
            String key = sourceType.FullName + destinationType.FullName + excludedProperties;

            MemberwiseCopyWithCheckDelegate memberwiseCopyWithCheckDelegate;

            if (!_memberwiseCopyWithCheckCache.TryGetValue(key, out memberwiseCopyWithCheckDelegate))
            {
                memberwiseCopyWithCheckDelegate = _GenerateMemberwiseCopyWithCheckDelegate(sourceType, destinationType, excludedProperties);
                _memberwiseCopyWithCheckCache[key] = memberwiseCopyWithCheckDelegate;
            }

            if (memberwiseCopyWithCheckDelegate == null)
                throw new NoCompatibleTypesException();
            
            return memberwiseCopyWithCheckDelegate;
        }

        private static MemberwiseCopyDelegate _GenerateMemberwiseCopyDelegate(Type sourceType, Type destinationType, String excludedProperties)
        {
            // Get a list of compatible properties (stop if entirely incompatible)
            List<MatchedProperty> matchedProperties = GetMatchedProperties(sourceType, destinationType, excludedProperties);
            if (matchedProperties.Count == 0) return null;

            DynamicMethod method = new DynamicMethod("", null, new Type[] { typeof(object), typeof(object) }, true);
            ILGenerator methodIL = method.GetILGenerator();

            // Transfer the arguments to the stack, cast them, and store them for later retrieval
            methodIL.DeclareLocal(sourceType);
            methodIL.DeclareLocal(destinationType);

            methodIL.Emit(OpCodes.Ldarg_0);                     // firstObject
            methodIL.Emit(OpCodes.Castclass, sourceType);       // cast sourceObject its proper Type
            methodIL.Emit(OpCodes.Stloc_0);                     // store
            methodIL.Emit(OpCodes.Ldarg_1);                     // secondObject
            methodIL.Emit(OpCodes.Castclass, destinationType);  // cast destinationObject to its proper Type
            methodIL.Emit(OpCodes.Stloc_1);                     // store
            
            // for the nullable types we have to declare local variables, so we
            // need to keep track of the count of these so that we reference
            // the right ones.
            int localCount = 2;

            foreach (MatchedProperty matchedProperty in matchedProperties)
            {
                _GenerateValueCopyStatement(matchedProperty, methodIL, ref localCount);
            }

            // return;
            methodIL.Emit(OpCodes.Ret);

            return (MemberwiseCopyDelegate)method.CreateDelegate(typeof(MemberwiseCopyDelegate));
        }
        
        private static MemberwiseCopyWithCheckDelegate _GenerateMemberwiseCopyWithCheckDelegate(Type sourceType, Type destinationType, String excludedProperties)
        {
            // Get a list of compatible properties (stop if entirely incompatible)
            List<MatchedProperty> matchedProperties = GetMatchedProperties(sourceType, destinationType, excludedProperties);
            if (matchedProperties.Count == 0) return null;

            DynamicMethod method = new DynamicMethod("", null, new Type[] { typeof(object), typeof(object) }, true);
            ILGenerator methodIL = method.GetILGenerator();

            // Transfer the arguments to the stack, cast them, and store them for later retrieval
            methodIL.DeclareLocal(sourceType);
            methodIL.DeclareLocal(destinationType);

            methodIL.Emit(OpCodes.Ldarg_0);                     // firstObject
            methodIL.Emit(OpCodes.Castclass, sourceType);       // cast sourceObject its proper Type
            methodIL.Emit(OpCodes.Stloc_0);                     // store
            methodIL.Emit(OpCodes.Ldarg_1);                     // secondObject
            methodIL.Emit(OpCodes.Castclass, destinationType);  // cast destinationObject to its proper Type
            methodIL.Emit(OpCodes.Stloc_1);                     // store

            // for the nullable types we have to declare local variables, so we
            // need to keep track of the count of these so that we reference
            // the right ones.
            int localCount = 2;

            foreach (MatchedProperty matchedProperty in matchedProperties)
            {
                Label equalLabel;
                Label notEqualLabel;

                // Generate the equals check IL for this matched property. If it
                // fails (probably because one or the other property is not
                // readable), then just skip ahead to the next property.
                if (_GenerateCheckEqualsStatement(matchedProperty, methodIL, out equalLabel, out notEqualLabel, ref localCount))
                {
                    // if not equal, do the copy
                    methodIL.MarkLabel(notEqualLabel);
                    _GenerateValueCopyStatement(matchedProperty, methodIL, ref localCount);

                    // if equal, do nothing
                    methodIL.MarkLabel(equalLabel);
                }
            }

            // return;
            methodIL.Emit(OpCodes.Ret);

            return (MemberwiseCopyWithCheckDelegate)method.CreateDelegate(typeof(MemberwiseCopyWithCheckDelegate));
        }

        #endregion

        #endregion

        #region MemberwiseEquals and Dynamic Method Generator

        // Hold a list of generated IMemberwiseEquals objects so that we don't have to generate them
        // each time that they are used.
        private static Dictionary<string, MemberwiseEqualsDelegate> _memberwiseEqualsCache = new Dictionary<String, MemberwiseEqualsDelegate>();

        #region Public MemberwiseEquals Methods

        /// <summary>
        /// Perform a property-by-property comparison of the values of two objects. Only compares
        /// properties that are value types or strings.
        /// </summary>
        /// <param name="firstObject">The first of the objects to compare.</param>
        /// <param name="secondObject">The second of the objects to compare.</param>
        /// <returns>True if the objects' values are equivalent. False otherwise.</returns>
        public static bool MemberwiseEquals(object firstObject, object secondObject)
        {
            try
            {
                return MemberwiseEquals(firstObject, secondObject, null);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Perform a property-by-property comparison of the values of two objects. Only compares
        /// properties that are value types or strings. Allows for certain properties to be 
        /// excluded from comparison.
        /// </summary>
        /// <param name="firstObject">The first of the objects to compare.</param>
        /// <param name="secondObject">The second of the objects to compare.</param>
        /// <param name="excludedProperties">A comma-separated string of property names that should be excluded
        /// from the comparison process.</param>
        /// <returns>True if the objects' values are equivalent. False otherwise.</returns>
        public static bool MemberwiseEquals(object firstObject, object secondObject, String excludedProperties)
        {
            // First, check for nulls
            if (firstObject == null) return secondObject == null;
            if (secondObject == null) return firstObject == null;

            try
            {
                MemberwiseEqualsDelegate memberwiseEqualsDelegate = _GetMemberwiseEqualsDelegate(firstObject.GetType(), secondObject.GetType(), excludedProperties);
                return memberwiseEqualsDelegate(firstObject, secondObject);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private MemberwiseEquals Methods

        // Delegate used to define the basic contract for all dynamically created
        // "MemberwiseEquals" objects.
        private delegate bool MemberwiseEqualsDelegate(object firstObject, object secondObject);

        private static MemberwiseEqualsDelegate _GetMemberwiseEqualsDelegate(Type firstObjectType, Type secondObjectType, String excludedProperties)
        {
            String key = firstObjectType.FullName + secondObjectType.FullName + excludedProperties;

            MemberwiseEqualsDelegate memberwiseEqualsDelegate;

            if (!_memberwiseEqualsCache.TryGetValue(key, out memberwiseEqualsDelegate))
            {
                memberwiseEqualsDelegate = _GenerateMemberwiseEqualsDelegate(firstObjectType, secondObjectType, excludedProperties);
                _memberwiseEqualsCache[key] = memberwiseEqualsDelegate;
            }

            if (memberwiseEqualsDelegate == null)
                throw new NoCompatibleTypesException();
            return memberwiseEqualsDelegate;
        }

        private static MemberwiseEqualsDelegate _GenerateMemberwiseEqualsDelegate(Type firstObjectType, Type secondObjectType, String excludedProperties)
        {
            // Get a list of compatible properties (stop if entirely incompatible)
            List<MatchedProperty> matchedProperties = GetMatchedProperties(firstObjectType, secondObjectType, excludedProperties);
            if (matchedProperties.Count == 0) return null;

            DynamicMethod method = new DynamicMethod("", typeof(bool), new Type[] { typeof(object), typeof(object) }, true);
            ILGenerator methodIL = method.GetILGenerator();

            // Transfer the arguments to the stack, cast them, and store them for later retrieval
            methodIL.DeclareLocal(firstObjectType);
            methodIL.DeclareLocal(secondObjectType);

            methodIL.Emit(OpCodes.Ldarg_0);                     // firstObject
            methodIL.Emit(OpCodes.Castclass, firstObjectType);  // cast firstObject its proper Type
            methodIL.Emit(OpCodes.Stloc_0);                     // store
            methodIL.Emit(OpCodes.Ldarg_1);                     // secondObject
            methodIL.Emit(OpCodes.Castclass, secondObjectType); // cast secondObject to its proper Type
            methodIL.Emit(OpCodes.Stloc_1);                     // store

            // for the nullable types we have to declare local variables, so we
            // need to keep track of the count of these so that we reference
            // the right ones.
            int localCount = 2;

            foreach (MatchedProperty matchedProperty in matchedProperties)
            {
                Label equalLabel;
                Label notEqualLabel;

                // Generate the equals check IL for this matched property. If it
                // fails (probably because one or the other property is not
                // readable), then just skip ahead to the next property.
                if (_GenerateCheckEqualsStatement(matchedProperty, methodIL, out equalLabel, out notEqualLabel, ref localCount))
                {

                    // if not equal set return value to false
                    methodIL.MarkLabel(notEqualLabel);                                      // not equal
                    methodIL.Emit(OpCodes.Ldc_I4_0);                                        // set return value to false
                    methodIL.Emit(OpCodes.Ret);                                             // return

                    // get's here to keep on truckin'
                    methodIL.MarkLabel(equalLabel);                                      // so far so good
                }
            }

            // if we get to this point, then assume equality
            methodIL.Emit(OpCodes.Ldc_I4_1);                            // set return value to true
            methodIL.Emit(OpCodes.Ret);                                 // return

            return (MemberwiseEqualsDelegate)method.CreateDelegate(typeof(MemberwiseEqualsDelegate));
        }

        #endregion

        #endregion

        #region Generate ValueCopy and CheckEquals Statements (shared IL generators)

        // Generate the IL for copying values between two matched properties.
        // MUST have declared an int localCount (initialized to 2 for a given
        // method) to represent the number of local variables created in the method.
        // Returns false if the method fails to generate the IL.
        private static bool _GenerateValueCopyStatement(MatchedProperty matchedProperty,
                                                        ILGenerator methodIL,
                                                        ref int localCount)
        {
            if (!matchedProperty.AreAssignable)
                return false;
            
            PropertyInfo sourceProperty = matchedProperty.FirstProperty;
            PropertyInfo destinationProperty = matchedProperty.SecondProperty;
            MethodInfo sourceGetter = sourceProperty.GetGetMethod();
            MethodInfo destinationSetter = destinationProperty.GetSetMethod();

            if (sourceGetter == null || destinationSetter == null)
                return false;

            // Emit either:
            //   destinationProperty = sourceProperty;
            // or:
            //   destinationProperty = sourceProperty.GetValueOrDefault();
            methodIL.Emit(OpCodes.Ldloc_1);                                     // destinationObject
            methodIL.Emit(OpCodes.Ldloc_0);                                     // sourceObject
            methodIL.EmitCall(OpCodes.Callvirt, sourceGetter, null);            // get value

            // if one but not both of the values are nullable, we either get
            // the default value of a source nullable or create a new nullable
            // to store in the destination
            if (matchedProperty.HasNullableType)
            {
                if (sourceProperty.IsNullable() && !destinationProperty.IsNullable())
                {
                    MethodInfo getValueMethod = sourceProperty.PropertyType.GetMethod("GetValueOrDefault", new Type[] { });
                    methodIL.DeclareLocal(sourceProperty.PropertyType);
                    methodIL.Emit(OpCodes.Stloc_S, localCount);                     // store
                    methodIL.Emit(OpCodes.Ldloca_S, localCount++);                  // sourceValueRef
                    methodIL.EmitCall(OpCodes.Call, getValueMethod, null);          // get value or default
                }
                else if (!sourceProperty.IsNullable() && destinationProperty.IsNullable())
                {
                    ConstructorInfo ci = destinationProperty.PropertyType.GetConstructor(new Type[] { sourceProperty.PropertyType });
                    methodIL.Emit(OpCodes.Newobj, ci);
                }
            }

            methodIL.EmitCall(OpCodes.Callvirt, destinationSetter, null);       // set value
            return true;
        }

        // Generate the IL for checking the equality of two matched properties.
        // MUST have declared two labels, equals and notEquals, prior to calling.
        // Also MUST have declared an int localCount (initialized to 2) to represent
        // the number of local variables created in the method.
        // Returns false if the method fails to generate the IL.
        private static bool _GenerateCheckEqualsStatement(MatchedProperty matchedProperty,
                                                          ILGenerator methodIL,
                                                          out Label equalLabel,
                                                          out Label notEqualLabel,
                                                          ref int localCount)
        {
            int firstObjectLoc = 0;
            int secondObjectLoc = 1;
            PropertyInfo firstProperty = matchedProperty.FirstProperty;
            PropertyInfo secondProperty = matchedProperty.SecondProperty;
            MethodInfo firstPropertyGetter = firstProperty.GetGetMethod();
            MethodInfo secondPropertyGetter = secondProperty.GetGetMethod();
            MethodInfo equalsMethod = null;
            MethodInfo equalityMethod = null;
            bool swapObjects = false;
            bool nullableToDefault = false;

            equalLabel = new Label();   // null label
            notEqualLabel = new Label();// null label
            
            if (firstPropertyGetter == null || secondPropertyGetter == null)
                return false;

            equalLabel = methodIL.DefineLabel();
            notEqualLabel = methodIL.DefineLabel();
            Label notNullLabel = methodIL.DefineLabel();

            // For non-value types, we must call the Equals method to test for equality,
            // but first we need to make sure that the first object isn't null.
            if (matchedProperty.HasReferenceType)
            {
                // If the first is a value type but the second isn't, swap the comparison order so that we call the 
                // .Equals method on the reference type.
                swapObjects = firstProperty.PropertyType.IsValueType;
                if (swapObjects)
                {
                    firstObjectLoc = 1;
                    firstProperty = matchedProperty.SecondProperty;
                    firstPropertyGetter = firstProperty.GetGetMethod();   // reset
                    secondObjectLoc = 0;
                    secondProperty = matchedProperty.FirstProperty;
                    secondPropertyGetter = secondProperty.GetGetMethod(); // reset
                }

                equalsMethod = firstProperty.PropertyType.GetMethod("Equals", new Type[] { secondProperty.PropertyType });

                // If the first property is a reference type and the second isn't AND the first is Null, then they aren't equal
                // if (firstProperty != null)
                if (secondProperty.PropertyType.IsValueType)
                {
                    methodIL.Emit(OpCodes.Ldloc_S, firstObjectLoc);                         // firstObject
                    methodIL.EmitCall(OpCodes.Callvirt, firstPropertyGetter, null);         // get value
                    methodIL.Emit(OpCodes.Brfalse_S, notEqualLabel);                        // check null
                }
                else
                {
                    // Otherwise, both must be null in order for them to be equal
                    // if (firstProperty == null && secondProperty == null)

                    methodIL.Emit(OpCodes.Ldloc_S, firstObjectLoc);                         // firstObject
                    methodIL.EmitCall(OpCodes.Callvirt, firstPropertyGetter, null);         // get value
                    methodIL.Emit(OpCodes.Brtrue_S, notNullLabel);                          // check null

                    methodIL.Emit(OpCodes.Ldloc_S, secondObjectLoc);                        // secondObject
                    methodIL.EmitCall(OpCodes.Callvirt, secondPropertyGetter, null);        // get value
                    methodIL.Emit(OpCodes.Brfalse_S, equalLabel);                           // null == null, continue
                    methodIL.Emit(OpCodes.Br_S, notEqualLabel);                             // not equal
                }
            }
            // For nullable types (Nullable'1<type> and thus Generic), we need to
            // do much the same as above as we check to see if they are both null or
            // both equal
            else if (matchedProperty.HasNullableType)
            {
                // If both aren't nullable, make sure the first one is.
                swapObjects = !firstProperty.IsNullable();
                if (swapObjects)
                {
                    firstObjectLoc = 1;
                    firstProperty = matchedProperty.SecondProperty;
                    firstPropertyGetter = firstProperty.GetGetMethod();   // reset
                    secondObjectLoc = 0;
                    secondProperty = matchedProperty.FirstProperty;
                    secondPropertyGetter = secondProperty.GetGetMethod(); // reset
                }

                nullableToDefault = !secondProperty.IsNullable();

                if (nullableToDefault)
                    equalsMethod = firstProperty.GetNonNullableType().GetMethod("Equals", new Type[] { secondProperty.PropertyType });
                else
                    equalsMethod = typeof(Object).GetMethod("Equals", new Type[] { typeof(Object) });

            }
            // Plain old boring value types are pretty easy since you don't have to call the
            // .Equals method. However, you do need to call the op_Equality method for non
            // primitive types, so yeah I guess this isn't easy either.
            else
            {
                // This may or may not return a value depending on whether the value type is native or not
                equalityMethod = firstProperty.PropertyType.GetMethod("op_Equality", new Type[] { firstProperty.PropertyType, secondProperty.PropertyType });
            }

            methodIL.MarkLabel(notNullLabel);

            // now either "firstObject == secondObject" or "firstObject.Equals(secondObject)"
            methodIL.Emit(OpCodes.Ldloc_S, firstObjectLoc);                         // firstObject
            methodIL.EmitCall(OpCodes.Callvirt, firstPropertyGetter, null);         // get value
            
            // for nullables, we need to get references to the values (why????).
            if (matchedProperty.HasNullableType)
            {
                methodIL.DeclareLocal(firstProperty.PropertyType);
                methodIL.Emit(OpCodes.Stloc_S, localCount);                         // store
                methodIL.Emit(OpCodes.Ldloca_S, localCount++);                      // firstValueRef

                // furthermore, if one but not both of the values are nullable, we
                // (decided to) compare the non-nullable type to the default value
                // of a null nullable...again with the reference to the value (???)
                if (nullableToDefault)
                {
                    MethodInfo getValueMethod = firstProperty.PropertyType.GetMethod("GetValueOrDefault", new Type[] { });
                    methodIL.DeclareLocal(firstProperty.GetNonNullableType());
                    methodIL.EmitCall(OpCodes.Call, getValueMethod, null);          // get value or default
                    methodIL.Emit(OpCodes.Stloc_S, localCount);                     // store
                    methodIL.Emit(OpCodes.Ldloca_S, localCount++);                  // firstValueRef
                }
            }
            methodIL.Emit(OpCodes.Ldloc_S, secondObjectLoc);                        // secondObject
            methodIL.EmitCall(OpCodes.Callvirt, secondPropertyGetter, null);        // get value

            // prior to calling the equals method for nullable types we have to
            // cast the second value as object form (box it) and then constrain
            // the equals method to operate, err, correctly I guess (unnecessary?)
            if (matchedProperty.HasNullableType && !nullableToDefault)
            {
                methodIL.Emit(OpCodes.Box, secondProperty.PropertyType);
                methodIL.Emit(OpCodes.Constrained, firstProperty.PropertyType);
            }

            // can perform a super-fast, native Ceq. 
            // Non primitaves use either the Equals (reference types) or the more
            // efficient op_Equality method (value types).
            if (equalsMethod != null)           // reference type
                if (matchedProperty.HasNullableType && !nullableToDefault)
                    methodIL.EmitCall(OpCodes.Callvirt, equalsMethod, null);        // test equality
                else
                    methodIL.EmitCall(OpCodes.Call, equalsMethod, null);            // test equality
            else if (equalityMethod != null)    // value type
                methodIL.EmitCall(OpCodes.Call, equalityMethod, null);              // test equality
            else                                // primitive
                methodIL.Emit(OpCodes.Ceq);                                         // test equality

            // if equal, try the next matched property set
            methodIL.Emit(OpCodes.Brtrue_S, equalLabel);                            // if equal, goto equalLabel

            // otherwise, not equal
            methodIL.Emit(OpCodes.Br_S, notEqualLabel);                             // unconditionally goto notEqualLabel

            return true;
        }

        #endregion

        #region Public Static Utility Methods

        /// <summary>
        /// A static utility method to dynamically create an instance of the passed 
        /// object type.
        /// </summary>
        /// <param name="type">The type to be created.</param>
        /// <returns>An instance of the requested type.</returns>
        public static Object CreateObjectInstance(Type type)
        {
            Object instance;
            try
            {
                instance = Activator.CreateInstance(type);
            }
            catch (Exception ex)
            {
                throw new ObjectCreationException("Unable to create an object from the class: " +
                                                  type.ToString(), ex);
            }
            return instance;
        }

        /// <summary>
        /// A static utility method to check whether a passed object is an array.
        /// </summary>
        /// <param name="toCheck">The object to check.</param>
        /// <returns>True if the passed object is an array. Otherwise, false.</returns>
        public static bool CheckIsArray(Object toCheck)
        {
            return toCheck != null && CheckIsArray(toCheck.GetType());
        }

        /// <summary>
        /// A static utility method to check whether a passed type is an array.
        /// </summary>
        /// <param name="toCheck">The type to check.</param>
        /// <returns>True if the type object is an array. Otherwise, false.</returns>
        public static bool CheckIsArray(Type toCheck)
        {
            return toCheck.IsArray;
        }

        /// <summary>
        /// A static utility method to check whether a passed object is a list (but not an array).
        /// </summary>
        /// <param name="toCheck">The object to check.</param>
        /// <returns>True if the passed object is a list or an array. Otherwise, false.</returns>
        public static bool CheckIsList(Object toCheck)
        {
            return toCheck != null && CheckIsList(toCheck.GetType());
        }

        /// <summary>
        /// A static utility method to check whether a passed type is a list (but not an array).
        /// </summary>
        /// <param name="toCheck">The type to check.</param>
        /// <returns>True if the passed type is a list or an array. Otherwise, false.</returns>
        public static bool CheckIsList(Type toCheck)
        {
            return typeof(IList).IsAssignableFrom(toCheck) && !toCheck.IsArray;
        }

        /// <summary>
        /// A static utility method to check whether a passed object is a list source (i.e., can be converted to a list
        /// using the GetList() method)).
        /// </summary>
        /// <param name="toCheck">The object to check.</param>
        /// <returns>True if the passed object is a list or an array. Otherwise, false.</returns>
        public static bool CheckIsListSource(Object toCheck)
        {
            return toCheck != null && CheckIsListSource(toCheck.GetType());
        }

        /// <summary>
        /// A static utility method to check whether a passed type is a list source (i.e., can be converted to a list
        /// using the GetList() method)).
        /// </summary>
        /// <param name="toCheck">The type to check.</param>
        /// <returns>True if the passed type is a list or an array. Otherwise, false.</returns>
        public static bool CheckIsListSource(Type toCheck)
        {
            return typeof(IListSource).IsAssignableFrom(toCheck);
        }

        /// <summary>
        /// A static utility method to check whether a passed object is a dictionary.
        /// </summary>
        /// <param name="toCheck">The object to check.</param>
        /// <returns>True if the passed object is a dictionary. Otherwise, false.</returns>
        public static bool CheckIsDictionary(Object toCheck)
        {
            return toCheck is IDictionary;
        }

        /// <summary>
        /// A static utility method to check whether a passed type is a dictionary.
        /// </summary>
        /// <param name="toCheck">The type to check.</param>
        /// <returns>True if the passed type is a dictionary. Otherwise, false.</returns>
        public static bool CheckIsDictionary(Type toCheck)
        {
            return typeof(IDictionary).IsAssignableFrom(toCheck);
        }

        /// <summary>
        /// A static utility method to check whether a passed object is a collection.
        /// </summary>
        /// <param name="toCheck">The object to check.</param>
        /// <returns>True if the passed object is a collection. Otherwise, false.</returns>
        public static bool CheckIsCollection(Object toCheck)
        {
            return toCheck is IEnumerable && !(toCheck is String);
        }

        /// <summary>
        /// A static utility method to check whether a passed type is a collection.
        /// </summary>
        /// <param name="toCheck">The type to check.</param>
        /// <returns>True if the passed type is a collection. Otherwise, false.</returns>
        public static bool CheckIsCollection(Type toCheck)
        {
            return typeof(IEnumerable).IsAssignableFrom(toCheck) && !typeof(String).IsAssignableFrom(toCheck);
        }

        /// <summary>
        /// A static utility method to check whether a passed object is a value-type or string.
        /// </summary>
        /// <param name="toCheck">The object to check.</param>
        /// <returns>True if the passed object is a value-type or string. Otherwise, false.</returns>
        public static bool CheckIsValueType(Object toCheck)
        {
            return toCheck != null && CheckIsValueType(toCheck.GetType());
        }

        /// <summary>
        /// A static utility method to check whether a passed type is a value-type or string.
        /// </summary>
        /// <param name="toCheck">The type to check.</param>
        /// <returns>True if the passed type is a value-type or string. Otherwise, false.</returns>
        public static bool CheckIsValueType(Type toCheck)
        {
            return toCheck.IsValueType || typeof(String).IsAssignableFrom(toCheck);
        }

        public static IList AsList(Object toConvert)
        {
            if (CheckIsList(toConvert))
                return (IList)toConvert;

            if (CheckIsListSource(toConvert))
                return ((IListSource)toConvert).GetList();

            throw new UnsupportedCollectionTypeException("Cannot convert " + toConvert.GetType().ToString() + " to an IList");
        }

        #endregion

        #region Exception / Warning Event Handler

        /// <summary>
        /// Used by the SetExceptionMode method. See the description of this
        /// method for more information.
        /// </summary>
        public enum ExceptionMode
        {
            RaiseEvent,
            ThrowException
        }
        
        /// <summary>
        /// Set the exception mode such that either problems cause C#
        /// exceptions to be thrown or warning events raised. The problems
        /// that generally trigger these exceptions/warnings are with
        /// with included/excluded properties not being found or being
        /// incompatible.
        /// </summary>
        /// <param name="exceptionMode">One of two possible enumerated values:
        /// RaiseEvent or ThrowException.</param>
        public static void SetExceptionMode(ExceptionMode exceptionMode)
        {
            _exceptionMode = exceptionMode;
        }
        private static ExceptionMode _exceptionMode = ExceptionMode.ThrowException;

        /// <summary>
        /// An event handler (delegate) of this type handles the UberObject's
        /// OnWarning event.
        /// </summary>
        /// <param name="message">A text description of the warning.</param>
        public delegate void UberObjectWarningHandler(String message);

        /// <summary>
        /// If UberObject is (statically) in the "RaiseEvent" exception mode,
        /// this event will be triggered instead of throwing an exception when 
        /// a problem is encountered (generally with included or excluded properties).
        /// </summary>
        public static event UberObjectWarningHandler OnWarning;

        private static void _OnWarning(String message)
        {
            if (OnWarning != null) OnWarning(message);
        }

        #endregion

    }

    #region Exception Classes

    /// <summary>
    /// Thrown if an instance of an object cannot be created. Check the message and innerException for
    /// details as to the cause of the exception.
    /// </summary>
    public class ObjectCreationException : Exception
    {
        public ObjectCreationException(String errorMessage, Exception innerException)
            : base(errorMessage, innerException) { }
    }

    /// <summary>
    /// Thrown if a call is made to a method that assumes that the UberObject is currently tracking
    /// changes. If caught, this means that either BeginTrackingChanges was never called or that
    /// StopTrackingChanges was called prior to calling a method that requires change tracking.
    /// </summary>
    public class NotTrackingChangesException : Exception { }

    /// <summary>
    /// Thrown if a call is made to BeginTrackingChanges when an object is already tracking changes.
    /// </summary>
    public class AlreadyTrackingChangesException : Exception { }

    /// <summary>
    /// Thrown if a call is made to Compare or Copy two entriely incompatible objects.
    /// </summary>
    public class NoCompatibleTypesException : Exception { }

    /// <summary>
    /// Thrown a property name is explicitly included or excluded but is not found in
    /// an copied / compared object.
    /// </summary>
    public class BadPropertyNameException : Exception
    {
        public BadPropertyNameException(String errorMessage)
            : base(errorMessage) { }
    }

    /// <summary>
    /// Thrown if an attempt is made to copy or compare unsupported or incompatible collection types.
    /// </summary>
    public class UnsupportedCollectionTypeException : Exception
    {
        public UnsupportedCollectionTypeException(String errorMessage)
            : base(errorMessage) { }
    }

    #endregion

    #region PropertyInfo Extensions

    public static class PropertyInfoExtensions
    {
        public static bool IsNullable(this PropertyInfo prop)
        {
            return prop.PropertyType.IsGenericType && prop.PropertyType.Name.ToLower().Contains("nullable");
        }

        public static Type GetNonNullableType(this PropertyInfo prop)
        {
            return prop.PropertyType.IsGenericType ? prop.PropertyType.GetGenericArguments()[0] : prop.PropertyType;
        }
    }

    #endregion

}
