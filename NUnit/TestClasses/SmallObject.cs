﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSharp
{
    class SmallObject : UberObject
    {
        private static Random rand = new Random();

        public int IntProp { get; set; }
        public long LongProp { get; set; }
        public String StringProp { get; set; }
        public Double DoubleProp { get; set; }
        public Decimal DecimalProp { get; set; }
        public Boolean BooleanProp { get; set; }
        public DateTime DateTimeProp { get; set; }
        public Guid GuidProp { get; set; }
        public byte[] ByteArrayProp { get; set; }

        public List<ListObject> ListProp { get; set; }

        public short ROProp { get; private set; } // JUST TO TEST FOR FUNCTIONAL PROBLEMS
        public short WOProp { private get; set; } // JUST TO TEST FOR FUNCTIONAL PROBLEMS

        public SmallObject() { }
        public SmallObject(bool prefill)
        {
            if (!prefill) return;

            IntProp = -1;
            LongProp = -1;
            StringProp = "ORIGINAL";
            DoubleProp = -1D;
            DecimalProp = -1;
            BooleanProp = false;
            DateTimeProp = new DateTime(0);
            GuidProp = Guid.Empty;
            ByteArrayProp = new byte[] { 1, 1, 1, 1, 1 };

            ListProp = new List<ListObject>()
            {
                new ListObject(true),
                new ListObject(true),
                new ListObject(true),
                new ListObject(true),
                new ListObject(true)
            };
        }

    }
}
