﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace SystemSharp
{
    [TestFixture]
    public class CloningTests
    {
        [SetUp]
        public void Init()
        {
        }

        [Test]
        public void CloneModerateObject()
        {
            ModerateObject mo = (new ModerateObject(true)).Clone() as ModerateObject;
            Assert.IsNotNull(mo);
        }

        [Test]
        public void CloneModerateObjectTestEqual()
        {
            ModerateObject mo = new ModerateObject(true);
            ModerateObject mo2 = mo.Clone() as ModerateObject;
            Assert.IsTrue(mo.DateTimeProp.Equals(mo2.DateTimeProp));
            Assert.IsTrue(mo.DoubleProp.Equals(mo2.DoubleProp));
            Assert.IsTrue(mo.GuidProp.Equals(mo2.GuidProp));
            Assert.IsFalse(mo.UberGUID.Equals(mo2.UberGUID));
            Assert.IsTrue(mo.StringProp.Equals(mo2.StringProp));
        }

        [Test]
        public void CloneModerateObjectTestEqualPreserveGuids()
        {
            ModerateObject mo = new ModerateObject(true);
            ModerateObject mo2 = mo.Clone(null, true) as ModerateObject;
            Assert.IsTrue(mo.DateTimeProp.Equals(mo2.DateTimeProp));
            Assert.IsTrue(mo.DoubleProp.Equals(mo2.DoubleProp));
            Assert.IsTrue(mo.GuidProp.Equals(mo2.GuidProp));
            Assert.IsTrue(mo.UberGUID.Equals(mo2.UberGUID));
            Assert.IsTrue(mo.StringProp.Equals(mo2.StringProp));
        }
            
        [Test]
        public void CloneModerateObjectTestEqualDeep()
        {
            ModerateObject mo = new ModerateObject(true);
            ModerateObject mo2 = mo.Clone("ByteArrayProp,DictionaryProp,ListProp,SmallObjectProp") as ModerateObject;
            Assert.IsTrue(mo.DateTimeProp.Equals(mo2.DateTimeProp));
            Assert.IsTrue(mo.DoubleProp.Equals(mo2.DoubleProp));
            Assert.IsTrue(mo.GuidProp.Equals(mo2.GuidProp));
            Assert.IsFalse(mo.UberGUID.EndsWith(mo2.UberGUID));
            Assert.IsTrue(mo.StringProp.Equals(mo2.StringProp));
            Assert.IsTrue(mo.ByteArrayProp.Length == mo2.ByteArrayProp.Length);
            Assert.IsTrue(mo.ByteArrayProp[2] == mo2.ByteArrayProp[2]);
            Assert.IsTrue(mo.ByteArrayProp[4] == mo2.ByteArrayProp[4]);
            Assert.IsTrue(mo.DictionaryProp.Count == mo2.DictionaryProp.Count);
            Assert.IsTrue(mo.DictionaryProp.Keys.ElementAt(2).Equals(mo2.DictionaryProp.Keys.ElementAt(2)));
            Assert.IsTrue(mo.DictionaryProp.Values.ElementAt(4).Equals(mo2.DictionaryProp.Values.ElementAt(4), null));
            Assert.IsTrue(mo.ListProp.Count == mo2.ListProp.Count);
            Assert.IsTrue(mo.ListProp[1].Equals(mo2.ListProp[1], null));
            Assert.IsTrue(mo.ListProp[3].Equals(mo2.ListProp[3], null));
        }


        [Test]
        [ExpectedException(typeof(BadPropertyNameException))]
        public void CloneSmallObjectTestExceptionCloneBadProperty()
        {
            UberObject.SetExceptionMode(UberObject.ExceptionMode.ThrowException);
            SmallObject so = new SmallObject(true);
            SmallObject so2 = so.Clone("BADPROPERTYNAME") as SmallObject;
        }

        [Test]
        [ExpectedException(typeof(UnsupportedCollectionTypeException))]
        public void CloneModerateObjectTestExceptionCloneUnsupported()
        {
            ModerateObject mo = new ModerateObject(true);
            mo.SmallObjectProp = new SmallObject(true);
            ModerateObject mo2 = mo.Clone("StackProp") as ModerateObject;
        }

        private static int warningCount = 0;
        public void ReceiveWarning(String message)
        {
            warningCount++;
        }

        [Test]
        public void CloneModerateObjectTestWarnings()
        {
            UberObject.SetExceptionMode(UberObject.ExceptionMode.RaiseEvent);
            UberObject.OnWarning += new UberObject.UberObjectWarningHandler(ReceiveWarning);
            warningCount = 0;

            ModerateObject mo = new ModerateObject(true);
            mo.SmallObjectProp = new SmallObject(true);
            
            // SHOULD THROW TWO WARNINGS FOR BADNAME AND UNSUPPORTED COLLECTION TYPE (STACK)
            ModerateObject mo2 = mo.Clone("BADNAME,StackProp,ListProp,SmallObjectProp,SmallObjectProp.ListProp") as ModerateObject;
            System.Threading.Thread.Sleep(10); // make sure we get back our warnings
            Assert.IsTrue(warningCount == 2);
            
            Assert.IsTrue(mo.DateTimeProp.Equals(mo2.DateTimeProp));
            Assert.IsTrue(mo.DoubleProp.Equals(mo2.DoubleProp));
            Assert.IsTrue(mo.GuidProp.Equals(mo2.GuidProp));
            Assert.IsFalse(mo.UberGUID.EndsWith(mo2.UberGUID));
            Assert.IsTrue(mo.StringProp.Equals(mo2.StringProp));
            Assert.IsTrue(mo2.ByteArrayProp == null);
            Assert.IsTrue(mo2.DictionaryProp == null);
            Assert.IsTrue(mo.ListProp.Count == mo2.ListProp.Count);
            Assert.IsTrue(mo.ListProp[1].Equals(mo2.ListProp[1], null));
            Assert.IsTrue(mo.ListProp[3].Equals(mo2.ListProp[3], null));
            Assert.IsTrue(mo.SmallObjectProp.Equals(mo2.SmallObjectProp, null));
            Assert.IsTrue(mo.SmallObjectProp.Equals(mo2.SmallObjectProp, "ListProp"));
            Assert.IsFalse(mo.SmallObjectProp.Equals(mo2.SmallObjectProp, "ByteArrayProp,ListProp"));
        }
    }
}
