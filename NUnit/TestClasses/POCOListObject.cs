﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSharp
{
    class POCOListObject
    {
        public Int32 IntProp { get; set; }
        public long LongProp { get; set; }
        public String StringProp { get; set; }

        public POCOListObject() { }
        public POCOListObject(bool prefill)
        {
            if (!prefill) return;

            IntProp = -1;
            LongProp = -1;
            StringProp = "ORIGINAL";
        }
    }
}
