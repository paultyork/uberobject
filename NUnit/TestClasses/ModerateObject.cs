﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSharp
{
    class ModerateObject : UberObject
    {
        private static Random rand = new Random();

        public Int32 IntProp { get; set; }
        public String StringProp { get; set; }
        public long LongProp { get; set; }
        public Double DoubleProp { get; set; }
        public Decimal DecimalProp { get; set; }
        public Boolean BooleanProp { get; set; }
        public DateTime DateTimeProp { get; set; }
        public Guid GuidProp { get; set; }
        public byte[] ByteArrayProp { get; set; }

        public SmallObject SmallObjectProp { get; set; }

        public List<ListObject> ListProp { get; set; }
        public Stack<ListObject> StackProp { get; set; }
        public Dictionary<String, ListObject> DictionaryProp { get; set; }

        public ModerateObject() { }
        public ModerateObject(bool prefill)
        {
            if (!prefill) return;

            IntProp = -1;
            StringProp = "ORIGINAL";
            LongProp = -1;
            DoubleProp = -1D;
            DecimalProp = -1;
            BooleanProp = false;
            DateTimeProp = new DateTime(0);
            GuidProp = new Guid("dddddddd-dddd-dddd-dddd-dddddddddddd");

            ByteArrayProp = new byte[] { 1, 1, 1, 1, 1 };

            SmallObjectProp = null;

            ListProp = new List<ListObject>()
                {
                    new ListObject(true),
                    new ListObject(true),
                    new ListObject(true),
                    new ListObject(true),
                    new ListObject(true)
                };

            // ******** WILL NOT BE COPIED/CLONED ********
            StackProp = new Stack<ListObject>();
            StackProp.Push(new ListObject(true));
            StackProp.Push(new ListObject(true));
            StackProp.Push(new ListObject(true));
            StackProp.Push(new ListObject(true));
            StackProp.Push(new ListObject(true));

            DictionaryProp = new Dictionary<string, ListObject>();
            DictionaryProp.Add("1", new ListObject(true));
            DictionaryProp.Add("2", new ListObject(true));
            DictionaryProp.Add("3", new ListObject(true));
            DictionaryProp.Add("4", new ListObject(true));
            DictionaryProp.Add("5", new ListObject(true));
        }

    }
}
