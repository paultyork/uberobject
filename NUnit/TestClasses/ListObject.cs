﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSharp
{
    class ListObject : UberObject
    {
        private static Random rand = new Random();

        public Int32 IntProp { get; set; }
        public Double LongProp { get; set; }
        public String StringProp { get; set; }

        public ListObject() { }
        public ListObject(bool prefill)
        {
            if (!prefill) return;

            IntProp = -1;
            LongProp = -1;
            StringProp = "ORIGINAL";
        }

    }
}
