﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSharp
{
    class POCOObject
    {
        public Int32 IntProp { get; set; }
        public long LongProp { get; set; }
        public String StringProp { get; set; }
        public Double DoubleProp { get; set; }
        public Decimal DecimalProp { get; set; }
        public Boolean BooleanProp { get; set; }
        public DateTime DateTimeProp { get; set; }
        public Guid GuidProp { get; set; }
        public byte[] ByteArrayProp { get; set; }

        public List<POCOListObject> ListProp { get; set; }

        public short ROProp { get; private set; } // JUST TO TEST FOR FUNCTIONAL PROBLEMS
        public short WOProp { private get; set; } // JUST TO TEST FOR FUNCTIONAL PROBLEMS

        public POCOObject() { }
        public POCOObject(bool prefill)
        {
            if (!prefill) return;

            IntProp = -1;
            LongProp = -1;
            StringProp = "ORIGINAL";
            DoubleProp = -1D;
            DecimalProp = -1;
            BooleanProp = false;
            DateTimeProp = new DateTime(0);
            GuidProp = new Guid("dddddddd-dddd-dddd-dddd-dddddddddddd");
            ByteArrayProp = new byte[] { 1, 1, 1, 1, 1 };

            ListProp = new List<POCOListObject>()
            {
                new POCOListObject(true),
                new POCOListObject(true),
                new POCOListObject(true),
                new POCOListObject(true),
                new POCOListObject(true)
            };
        }

    }
}
