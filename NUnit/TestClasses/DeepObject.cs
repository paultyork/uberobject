﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SystemSharp
{
    class DeepObject : UberObject
    {
        private static Random rand = new Random();

        public Int32 IntProp { get; set; }
        public String StringProp { get; set; }
        public long LongProp { get; set; }
        public Double DoubleProp { get; set; }
        public Decimal DecimalProp { get; set; }
        public Boolean BooleanProp { get; set; }
        public DateTime DateTimeProp { get; set; }
        public Guid GuidProp { get; set; }
        public byte[] ByteArrayProp { get; set; }
        public String[] StringArrayProp { get; set; }
        public SmallObject[] SmallObjectArrayProp { get; set; }

        public SmallObject SmallObjectProp { get; set; }

        public List<SmallObject> ListProp { get; set; }
        public Dictionary<String, SmallObject> DictionaryProp { get; set; }

        public DeepObject() { }
        public DeepObject(bool prefill)
        {
            if (!prefill) return;

            IntProp = -1;
            StringProp = "ORIGINAL";
            LongProp = -1;
            DoubleProp = -1D;
            DecimalProp = -1;
            BooleanProp = false;
            DateTimeProp = new DateTime(0);
            GuidProp = new Guid("dddddddd-dddd-dddd-dddd-dddddddddddd");
            ByteArrayProp = new byte[] { 1, 1, 1, 1, 1 };
            StringArrayProp = new String[] { "ORIGINAL", "ORIGINAL", "ORIGINAL", "ORIGINAL", "ORIGINAL" };
            SmallObjectArrayProp = new SmallObject[]
                {
                    new SmallObject(true),
                    new SmallObject(true),
                    new SmallObject(true),
                    new SmallObject(true),
                    new SmallObject(true)
                };


            SmallObjectProp = new SmallObject(true);

            ListProp = new List<SmallObject>()
                {
                    new SmallObject(true),
                    new SmallObject(true),
                    new SmallObject(true),
                    new SmallObject(true),
                    new SmallObject(true)
                };

            DictionaryProp = new Dictionary<string, SmallObject>();
            DictionaryProp.Add("1", new SmallObject(true));
            DictionaryProp.Add("2", new SmallObject(true));
            DictionaryProp.Add("3", new SmallObject(true));
            DictionaryProp.Add("4", new SmallObject(true));
            DictionaryProp.Add("5", new SmallObject(true));
        }
    }
}
