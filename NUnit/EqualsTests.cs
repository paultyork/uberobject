﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace SystemSharp
{
    [TestFixture]
    public class EqualsTests
    {
        [SetUp]
        public void Init()
        {
        }

        [Test]
        public void CompareSmallObjectToSmallObjectShallow()
        {
            SmallObject so = new SmallObject(true);
            SmallObject so2 = new SmallObject(true);
            Assert.IsTrue(so.Equals(so2, null));
            so.ByteArrayProp[4] = 1;
            Assert.IsTrue(so.Equals(so2, null));
            so.ListProp.RemoveAt(4);
            Assert.IsTrue(so.Equals(so2, null));
        }

        [Test]
        public void CompareSmallObjectToSmallObjectShallowModifyLong()
        {
            SmallObject so = new SmallObject(true);
            Assert.IsTrue(so.Equals(new SmallObject(true), null));
            so.LongProp = 100;
            Assert.IsFalse(so.Equals(new SmallObject(true), null));
        }

        [Test]
        public void CompareSmallObjectToSmallObjectDeepModifyByteArray()
        {
            SmallObject so = new SmallObject(true);
            Assert.IsTrue(so.Equals(new SmallObject(true), "ByteArrayProp"));
            so.ByteArrayProp[4] = 100;
            Assert.IsFalse(so.Equals(new SmallObject(true), "ByteArrayProp"));
        }

        [Test]
        public void CompareSmallObjectToSmallObjectDeepModifyListSize()
        {
            SmallObject so = new SmallObject(true);
            Assert.IsTrue(so.Equals(new SmallObject(true), "ListProp"));
            so.ListProp.RemoveAt(4);
            Assert.IsFalse(so.Equals(new SmallObject(true), "ListProp"));
        }

        [Test]
        public void CompareSmallObjectToSmallObjectDeepModifyListItemValue()
        {
            SmallObject so = new SmallObject(true);
            Assert.IsTrue(so.Equals(new SmallObject(true), "ListProp"));
            so.ListProp[4].LongProp = 100;
            Assert.IsFalse(so.Equals(new SmallObject(true), "ListProp"));
        }

        [Test]
        public void CompareNullableSmallObjectToSmallObjectShallow()
        {
            SmallObject so = new SmallObject(true);
            NullableSmallObject nso = new NullableSmallObject(true);
            Assert.IsTrue(so.Equals(nso, null));
            so.ByteArrayProp[4] = 1;
            Assert.IsTrue(so.Equals(nso, null));
            so.ListProp.RemoveAt(4);
            Assert.IsTrue(so.Equals(nso, null));
        }

        [Test]
        public void CompareSmallObjectToNullableSmallObjectShallow()
        {
            SmallObject so = new SmallObject(true);
            NullableSmallObject nso = new NullableSmallObject(true);
            Assert.IsTrue(nso.Equals(so, null));
            nso.ByteArrayProp[4] = 1;
            Assert.IsTrue(nso.Equals(so, null));
            nso.ListProp.RemoveAt(4);
            Assert.IsTrue(nso.Equals(so, null));
        }

        [Test]
        public void CompareNullableSmallObjectToSmallObjectShallowModifyLong()
        {
            SmallObject so = new SmallObject(true);
            NullableSmallObject nso = new NullableSmallObject(true);
            Assert.IsTrue(nso.Equals(so, null));
            nso.LongProp = null;
            Assert.IsFalse(nso.Equals(so, null));
        }

        [Test]
        public void CompareSmallObjectToAnonymousObjectShallow()
        {
            SmallObject so = new SmallObject(true);
            var anon = new { IntProp = (int)-1, LongProp = (long)-1 };
            Assert.IsTrue(so.Equals(anon, null));
            so.LongProp = 100;
            Assert.IsFalse(so.Equals(anon, null));
        }

        [Test]
        public void CompareNullableSmallObjectToAnonymousObjectShallow()
        {
            NullableSmallObject nso = new NullableSmallObject(true);
            var anon = new { IntProp = (int)-1, LongProp = (long)-1 };
            Assert.IsTrue(nso.Equals(anon, null));
            nso.LongProp = null;
            Assert.IsFalse(nso.Equals(anon, null));
        }

        [Test]
        public void CompareNullableAnonymousObjectToAnonymousObjectShallow()
        {
            var anon1 = new { IntProp = (int?)-1, LongProp = (long)-1 };
            var anon2 = new { IntProp = (int)-1, LongProp = (long)-1 };
            Assert.IsTrue(UberObject.Equals(anon1, anon2, null));
            var anon3 = new { IntProp = (int?)null, LongProp = (long)-1 };
            Assert.IsFalse(UberObject.Equals(anon2, anon3, null));
        }

        [Test]
        public void CompareSmallObjectToModerateObjectShallow()
        {
            SmallObject so = new SmallObject(true);
            ModerateObject mo = new ModerateObject(true);

            Assert.IsFalse(so.Equals(mo, null));
            Assert.IsFalse(mo.Equals(so, null));
            
            mo.GuidProp = Guid.Empty; // reset to so's value
            
            Assert.IsTrue(so.Equals(mo, null));
            Assert.IsTrue(mo.Equals(so, null));
            
            mo.LongProp = 100;
            
            Assert.IsFalse(so.Equals(mo, null));
            Assert.IsFalse(mo.Equals(so, null));
        }
    }
}
